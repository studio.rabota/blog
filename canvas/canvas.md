# Drawing Malevich, Míro and Appel on canvas

This article will show the basics of drawing on the `<canvas>` element.

First create an html5 document and add the `<canvas>` element.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <canvas id="canvas"></canvas>
    <script>
        // The rest of the code goes here..        
    </script>
</body>
</html>
```

We'll set the size of the canvas element first. 

```js
const canvas = document.getElementById('canvas')
canvas.width = 500;
canvas.height = 500;
```

We can then get the context from the canvas using `getContext('2d')`.

```js
const context = canvas.getContext('2d')
```
> Another option would be to use `getContext('webgl')`, allowing us to draw in 3d space.

Alright, back to kindergarten.. Let's draw our first line.

```js
// Start a path
context.beginPath() 
// Move the position to x: 50, y: 50
context.moveTo(50, 50) 
// Draw a line from the current position to x: 100, y: 50
context.lineTo(100, 50) 
// Add the line to the canvas
context.stroke()
```

Nice, since we can draw a line, we can draw a rectangle.

```js
context.beginPath()
context.moveTo(50, 50)
context.lineTo(100, 50)
context.lineTo(100, 100)
context.lineTo(50, 100)
context.moveTo(50, 50)
context.stroke()
```

A neat little trick to close paths is the method `closePath`.

```js
context.beginPath()
context.moveTo(50, 50)
context.lineTo(100, 50)
context.lineTo(100, 100)
context.closePath() // Replaced moveTo(50, 50) with closePath
context.stroke()
```

And we can fill the rectangle instead of drawing just a line using the method `fill` instead of `stroke`.

```js
context.beginPath()
context.moveTo(50, 50)
context.lineTo(100, 50)
context.lineTo(100, 100)
context.lineTo(50, 100)
context.closePath()
context.fill()
```

Let's not make our life difficult. We can draw a rectangle in two lines using the `rect` method.

```js
context.rect(50, 50,50,50)
context.fill()
```

Hack! We can draw a rectangle in one line of code, using `fillRect`.

```js
context.fillRect(50, 50,50,50)
```

Or us `strokeRect` for a line based rectangle.

```js
context.strokeRect(50, 50, 50, 50)
```

We're artists, we don't do just square rectangles (except Malevich). Let's draw something curvy, a sine wave. 

The code below draws a curve by drawing small lines. Try logging `xyPoints` with `console.log` to understand how the curves are drawn. 

Also, just for fun, play around with the `frequency` value. Personally my favorite drawing is created when setting `frequency` to `2`.

```js
const baseLine = 100
const frequency = 0.05
const xPoints = [...Array(300).keys()]

context.beginPath()
context.moveTo(0, baseLine)

const xyPoints = xPoints.map(x => ({ 
    x, 
    y: baseLine + Math.sin(x * frequency) * 100
}))

xyPoints.forEach(({x, y}) => {
    context.lineTo(x, y)  
})

context.stroke()
```

Creating curved lines by drawing lots of small lines is not very efficient. Luckily, we can use the `quadraticCurveTo` method. A line drawn with `quadraticCurveTo` consists of 3 points: 
1. starting point
2. end point 
3. point towards which the lines curves.

Do play around with the xy positions of each point to understand how the line is drawn.

```js
const startPoint = {
    x: 50,
    y: 50
}
const curveToPoint = {
    x: 300,
    y: 100
}
const endPoint = {
    x: 90,
    y: 90
}

context.beginPath()
context.moveTo(startPoint.x, startPoint.y)
context.quadraticCurveTo(curveToPoint.x, curveToPoint.y, endPoint.x, endPoint.y)
context.stroke()

context.fillRect(startPoint.x - 4, startPoint.y - 4, 8, 8)
context.fillRect(curveToPoint.x - 4, curveToPoint.y - 4, 8, 8)
context.fillRect(endPoint.x - 4, endPoint.y - 4, 8, 8)
```

Now let's create some random art with the `quadraticCurveTo`. We'll use `Math.random()` to create random points.

Do play around with the `amountOfLines` value. The number `10` will get you a nice Miró, set it to a `100` when Karel Appel appeals to you more.

And make it more Malevichy by changing `stroke()` to `fill()`.

```js
const size = 300
const amountOfLines = 10

const lines = [...Array(amountOfLines).keys()]

const xyPoints = lines.map(() => ({
    startPoint: {
        x: Math.random() * size,
        y: Math.random() * size
    },
    endPoint: {
        x: Math.random() * size,
        y: Math.random() * size
    },
    curveToPoint: {
        x: Math.random() * size,
        y: Math.random() * size
    }
}))

xyPoints.forEach(({
    startPoint,
    endPoint,
    curveToPoint
}) => {
    context.beginPath()
    context.moveTo(startPoint.x, startPoint.y)
    context.quadraticCurveTo(endPoint.x, endPoint.y, curveToPoint.x, curveToPoint.y)
    context.stroke()
})
```

Okay, you artist, time to shop for some more utensils. Let's look at `bezierCurveTo`. This is like `quadraticCurveTo` except you add one more point to which the line can curve.

```js
const startPoint = {
    x: 50,
    y: 50
}
const curveToPoint = {
    x: 300,
    y: 100
}
const curveToSecondPoint = {
    x: 150,
    y: 370
}
const endPoint = {
    x: 500,
    y: 500
}

context.beginPath()
context.moveTo(startPoint.x, startPoint.y)
context.bezierCurveTo(curveToPoint.x, curveToPoint.y, curveToSecondPoint.x, curveToSecondPoint.y,  endPoint.x, endPoint.y)
context.stroke()

context.fillRect(startPoint.x - 4, startPoint.y - 4, 8, 8)
context.fillRect(curveToPoint.x - 4, curveToPoint.y - 4, 8, 8)
context.fillRect(curveToSecondPoint.x - 4, curveToSecondPoint.y - 4, 8, 8)
context.fillRect(endPoint.x - 4, endPoint.y - 4, 8, 8)
```
## Black Circle

![Kazimir Malevich Black Circle](./img/Black_circle.jpg)

> Kazimir Malevich, Public domain, via Wikimedia Commons

There's no such thing as a `circle` method, however we can use the `arc` method.

The `startAngle` and `endAngle` values are in radians. A half circle would for example be `Math.PI` (`Math.PI = 3.141592653589793`). To calculate a full circle whe can set the `endAngle` to `Math.PI * 2`. 

```js
context.beginPath()
const x = 300
const y = 100
const radius = 100
const startAngle = 0
const endAngle = Math.PI * 2
context.arc(x, y, radius, startAngle, endAngle)
context.fill()
```

The `arc` method has one more argument which determines the direction the `arc` is drawn in. To draw the line counter clockwise you can set the 6th argument to `true`.

```js
context.beginPath()
const x = 300
const y = 100
const radius = 100
const startAngle = 0
const endAngle = 1.5
const counterClockWise = true
context.arc(x, y, radius, startAngle, endAngle, counterClockWise)
context.stroke()
```

## Colors and Lines

![Kleurenzeefdruk_Appel](./img/Kleurenzeefdruk_Appel.jpg)

> Karel Appel and the CODA Museum, CC BY 3.0 <https://creativecommons.org/licenses/by/3.0>, via Wikimedia Commons

Just black and white will do when drawing Malevich. Appel requires a bit of color. We can do this with `fillStyle` and `strokeStyle`.

```js
context.fillStyle = 'yellow'
context.strokeStyle = 'red'

context.beginPath()
context.rect(250, 100, 100, 100)
context.stroke()
context.fill()
```

Let's get a bigger brush by setting `lineWidth`.

```js
context.fillStyle = 'yellow'
context.strokeStyle = 'red'
context.lineWidth = 10

context.beginPath()
context.rect(250, 100, 100, 100)
context.stroke()
context.fill()
```

The order of `stroke` and `fill` above matters. Notice how the example below renders differently.

```js
context.fillStyle = 'yellow'
context.strokeStyle = 'red'
context.lineWidth = 10

context.beginPath()
context.rect(250, 100, 100, 100)
context.fill()
context.stroke()
```

We can also decide how the line ends. The options are:
1. butt; is the default
2. round; rounds off the ends of the line
3. square; takes into account the width of the line

```js
context.lineCap = 'round'
context.lineWidth = 10
context.beginPath()
context.moveTo(50, 50)
context.lineTo(100, 50)
context.stroke()
```

The `lineCap` is applied to line endings. Use `lineJoin` for places where two lines meet.

Here again we have 3 options:
1. miter; is the default
2. round; rounds off the ends of the line
3. bevel; creates a sharp edge

```js
context.lineJoin = 'round'
context.lineWidth = 10
context.beginPath()
context.moveTo(50, 50)
context.lineTo(100, 50)
context.lineTo(100, 100)
context.stroke()
```

The default `lineJoin` of `miter` has an additional option called `miterLimit`. This determines the cut off point of the corner (how sharp the point can be).

Notice for example how the join of the first line seems cutt off. This is becaus the `miterLimit` is set to `2` and the angle it to sharp. The second line has a `miterLimit` of `15` and shows a nice sharp corner.

```js
context.lineJoin = 'miter'
context.lineWidth = 20

context.miterLimit = 2
context.beginPath()
context.moveTo(50, 50)
context.lineTo(100, 50)
context.lineTo(50, 100)
context.stroke()

context.miterLimit = 15
context.beginPath()
context.moveTo(150, 50)
context.lineTo(200, 50)
context.lineTo(150, 100)
context.stroke()
```

Appel can do with some fills, ... however needs some gradients. We can do this with `createLinearGradient`.

The first two params in `createLinearGradient` defines the point where the gradient starts and the last two the point where the gradient ends. In this case from point `x: 100`, `y: 100` to `x: 100`, `y: 300`.

You can add as many `addColorStop` as you like. The first param determines the position (between `0` and `1`) and the second the color.

Almost like a sunset, pretty cool! Play around with the `createLinearGradient` and `addColorStop` to make your own gradient.

```js
const gradient = context.createLinearGradient(100, 100, 100, 300)
gradient.addColorStop(0, "purple")
gradient.addColorStop(0.5, "hotpink")
gradient.addColorStop(1, "aliceblue")

context.fillStyle = gradient
context.fillRect(100, 100, 300, 300)
```

Let's create a purple sun thingy with `createRadialGradient`. 

The first two arguments is a point, the third argument is the radius of around that point. Then another two arguments to set a point and a radius around that point. Confusing?! Best is to just play around with the example.

```js
const gradient = context.createRadialGradient(100, 100, 0, 100, 100, 100)
gradient.addColorStop(0, "purple")
gradient.addColorStop(1, "hotpink")

context.fillStyle = gradient
context.beginPath()
context.arc(100, 100, 100, 0, Math.PI * 2, false)
context.fill()
```

By moving the shade a bit we can make the circle look like a sphere. Waaat. Yess.

```js
const gradient = context.createRadialGradient(120, 80, 0, 110, 90, 100)
gradient.addColorStop(0, "white")
gradient.addColorStop(1, "black")

context.fillStyle = gradient
context.beginPath()
context.arc(100, 100, 100, 0, Math.PI * 2, false)
context.fill()
```

## Text

Adding text is super simple! Write your poem and decide where the text should be positioned. 

```js
context.fillText("Art", 100, 100)
```

Not really readable. Let's set the font size and font type.

```js
context.font = "italic bold 50px Helvetica"
context.fillText("Art", 100, 100)
```

As an alternative you can use `strokeText` instead of `fillText`.

```js
context.font = "italic bold 50px Helvetica"
context.strokeText("Art", 100, 100)
```

Or you can combine them.

```js
context.font = "italic bold 50px Helvetica"
context.fillStyle = "purple"
context.strokeStyle = "hotpink"
context.lineWidth = 5;
context.strokeText("Art", 100, 100)
context.fillText("Art", 100, 100)
```

The `xy` of the text is by default on the right bottom position of the text. It is possible to change this by using `textAlign`.

```js
context.font = "italic bold 50px Helvetica"
context.textAlign = "center"
context.fillText("Art", 100, 100)
context.textAlign = "right"
context.fillStyle = "lightgray"
context.fillText("Art", 100, 150)
context.textAlign = "left"
context.fillStyle = "gray"
context.fillText("Art", 100, 200)
```

This makes it easier to place for example something on the right or center.

```js
context.font = "50px Helvetica"
context.textAlign = "right"
context.fillText("Art", canvas.width, 100)
context.textAlign = "center"
context.fillText("Art", canvas.width / 2, 100)
```

You can also adjust the baseline of the text. The baseline of the second text is set to top.

Try out some of the other options as well:
- hanging
- middle
- alphabetic
- ideographic
- bottom

```js
context.beginPath()
context.moveTo(0, 100)
context.lineTo(500, 100)
context.setLineDash([5, 10])
context.stroke()

context.font = "italic bold 20px Helvetica"
context.fillText("als ik de waterpijp hoor borr'len", 100, 100)
context.textBaseline = "top"
context.fillStyle = "pink"
context.fillText("als ik de waterpijp hoor borr'len", 100, 100)
```

The method `measureText` can be used to retreive additional information. With this method you can for example get the width of the text. This is useful for adding additional graphics around the text.

```js
context.setLineDash([5, 5])

context.font = "italic bold 50px Helvetica"
context.fillText("waterpijp", 100, 100)

const metrics = context.measureText("waterpijp")

context.strokeRect(100, 100, metrics.width, -50)
```

## Images

The `drawImage` method allows you to add images onto the canvas. First the image tag needs to be created, either by adding an `<img />` to the HTML or by creating an img element via Javascript.

We need to make sure the image is loaded before rendering the image to the

```js
const image = document.createElement("img")
image.src = 'img/Black_circle.jpg'
image.addEventListener("load", () => {
    context.drawImage(image)
})
```

Additionally one can set the position and size of the image.

```js
const image = document.createElement("img")
image.src = 'img/Black_circle.jpg'
image.addEventListener("load", () => {
    context.drawImage(image, 20, 100, 50, 50)
})
```

It is also possible to clip the image. The first 4 numbers sets the postion and size of the src image. The other 4 position the area and sets the draw area.

```js
const image = document.createElement("img")
image.src = 'img/Black_circle.jpg'
image.addEventListener("load", () => {
    context.drawImage(image, 0, 0, 200, 200, 0, 0, 100, 100)
})
```

Cool thing is.. you can also make an image from a canvas drawing.

```js
context.fillRect(250, 100, 100, 100)

const image = document.createElement("img")
image.src = canvas.toDataURL()
document.body.appendChild(image)
```

## Canvas effects

By setting the `globalAlpha` you can determine how the next drawing is drawn over the existing content.

```js
context.fillStyle = "purple"
context.fillRect(50, 50, 100, 200)

context.globalAlpha = 0.5

context.fillStyle = "hotpink"
context.fillRect(50, 50, 200, 100)
```

There's a whole area of drawing and blending modes available via `globalCompositeOperation`. See [MDN](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation) for a full overview.

Setting `globalCompositeOperation` to `destination-over` will change the layer in which the next drawing is made. Notice how the `hotpink` rectangle is drawn behind the `purple` rectangle.

```js
context.fillStyle = "purple"
context.fillRect(50, 50, 100, 200)

context.globalCompositeOperation = "destination-over"

context.fillStyle = "hotpink"
context.fillRect(50, 50, 200, 100)
```

Setting `globalCompositeOperation` to `source-out` will create a kind off cut out effect. Also try out `source-in` which does the opposite. And while your at it, try `destination-out` and `destination-in`.

```js
context.fillStyle = "purple"
context.fillRect(50, 50, 100, 200)

context.globalCompositeOperation = "source-out"

context.fillStyle = "hotpink"
context.fillRect(0, 0, 300, 300)
```

A mask like effect can be achieved with `destination-atop` and `source-atop`.

```js
context.fillStyle = "purple"
context.fillRect(50, 50, 100, 200)

context.globalCompositeOperation = "destination-atop"

context.fillStyle = "hotpink"
context.fillRect(0, 100, 300, 100)
```

A really cool one is `xor`. 

```js
context.fillStyle = "purple"
context.fillRect(50, 50, 100, 200)

context.globalCompositeOperation = "xor"

context.fillStyle = "hotpink"
context.fillRect(0, 100, 300, 100)
```

## Shadows

Shadows are easily added by combining `shadowColor`, `shadowOffsetX` and `shadowOffsetY`.

```js
context.shadowColor = "rgba(0,0,0,0.5)"
context.shadowBlur = 10
context.shadowOffsetX = 10
context.shadowOffsetY = 10
context.fillStyle = "purple"
context.fillRect(50, 50, 100, 200)
```

## Transformations

Using `context.translate` you can change the origin point from which the drawing is made. By default the drawing starts at `0, 0`. Notice how these rectangles are drawn at `0,0`, because the origin has changed by `translate` the rectangles end up in a different position.

```js
context.translate(60,60)
context.fillRect(0,0,50,50)
context.translate(90,90)
context.fillRect(0,0,50,50)
```

Use `context.save` and `context.restore` to start the next drawing at the original origin.

```js
context.save()
context.translate(90,90)
context.fillRect(0,0,50,50)
context.restore()
context.fillRect(0,0,30,30)
```

You can also scale the drawing with `context.scale`.

```js
context.fillRect(0,0,50,50)
context.translate(90,90)
context.scale(2, 2)
context.fillRect(0,0,50,50)
```

And with `context.rotate` you can make beauties like these.

```js
const circles = [...Array(20).keys()]
context.translate(300, 300);

circles.forEach((value) => {
    context.rotate(Math.PI * 2 / circles.length);
    context.beginPath();
    context.arc(200, 0, 10, 0, Math.PI * 2);
    context.fill();
})
```

## Data

With `context.getImageData` you can get the red, green, blue and alpha value of each pixels. 

The index of a pixel can be found by the formula `(y * imageData.width + x) * 4`. It is times 4 because red, green, blue and alpha each have there own index. In this example the `imageData.data` array has `2.560.000` values (`800 * 800 * 4`).

```js
context.fillStyle = "blue"
context.fillRect(0, 0, 800, 800)

const imageData = context.getImageData(0, 0, 800, 800)
const colorAtX = 50
const colorAtY = 100
const index = (colorAtY * imageData.width + colorAtX) * 4

console.log("red: " + imageData.data[index])
console.log("green: " + imageData.data[index + 1])
console.log("blue: " + imageData.data[index + 2])
console.log("alpha: " + imageData.data[index + 3])
```

You can also draw each pixel by getting imageData with `context.getImageData` and manipulating the values. To render the changed `imageData` we call `context.putImageData`.

```js
const xPoints = [...Array(400).keys()]
const yPoints = [...Array(400).keys()]
const imageData = context.getImageData(0, 0, 800, 800)
xPoints.forEach(x => {
    yPoints.forEach(y => {
        const index = (y * imageData.width + x) * 4
        imageData.data[index] = Math.round(Math.random() * 255)
        imageData.data[index + 1] = Math.round(Math.random() * 255)
        imageData.data[index + 2] = Math.round(Math.random() * 255)
        imageData.data[index + 3] = 255
    })
})
context.putImageData(imageData, 0, 0)
```

It is also possible to create an empty imageData object by using `createImageData`. Here we are drawing `5000` random colored pixels in random places.

```js
context.fillRect(0, 0, 800, 800);

const points = [...Array(5000).keys()]
points.forEach(() => {
    var x = Math.round(Math.random() * 800),
        y = Math.round(Math.random() * 800);
    
    const imageData = context.createImageData(1, 1);
    imageData.data[0] = Math.round(Math.random() * 255);
    imageData.data[1] = Math.round(Math.random() * 255);
    imageData.data[2] = Math.round(Math.random() * 255);
    imageData.data[3] = 255;
    
    context.putImageData(imageData, x, y);
})
```

By using `requestAnimationFrame` with the functions above we can now create animations. Trippy!

```js
let angle = 0

const draw = () => {
    context.clearRect(0, 0, 500, 500)
    context.save()
    context.translate(300, 300)
    const scale = Math.sin(angle) + 1
    context.scale(scale, scale)
    context.rotate(angle)
    context.fillRect(-50, -50, 100, 100)
    angle += 0.01
    context.restore()
    requestAnimationFrame(draw)
}

draw()
```

## Events

By listening to events we can create some interaction. 

We subtract the position of the canvas element in relation to the document by using info from `canvas.getBoundingClientRect`.

Let's draw a square on each postion the mouse is clicked.

```js
const canvasBounding = canvas.getBoundingClientRect();
canvas.addEventListener("mousedown", onMouseDown);

function onMouseDown(event) {
    context.fillRect(
        event.clientX - canvasBounding.left, 
        event.clientY - canvasBounding.top, 
        100, 
        100
    )
}
```

And here's a simple free-hand drawing program.

```js
const rect = canvas.getBoundingClientRect()
let mouseX
let mouseY

context.lineWidth = 10

const onMouseDown = (event) => {
    mouseX = event.clientX - rect.left
    mouseY = event.clientY - rect.top
    canvas.addEventListener("mousemove", onMouseMove)
    document.body.addEventListener("mouseup", onMouseUp)
}

const onMouseMove = (event) => {
    context.beginPath()
    context.moveTo(mouseX, mouseY)
    mouseX = event.clientX - rect.left
    mouseY = event.clientY - rect.top
    context.lineTo(mouseX, mouseY)
    context.stroke()
}

const onMouseUp = (event) => {
    canvas.removeEventListener("mousemove", onMouseMove)
    document.body.removeEventListener("mouseup", onMouseUp)
}

canvas.addEventListener("mousedown", onMouseDown)
```

You can also listen to key event. 

It is best to place the key event listener on the body instead of the canavas (since the canvas does not have focus).

```js
document.body.addEventListener("keydown", (event) => {
    switch(event.code) {
        case "KeyC": {
            context.beginPath()
            const x = Math.round(Math.random() * 600)
            const y = Math.round(Math.random() * 600)
            const radius = Math.round(Math.random() * 100)
            const startAngle = 0
            const endAngle = Math.PI * 2
            context.arc(x, y, radius, startAngle, endAngle)
            context.fill()
            break
        }   
        case "KeyR": {
            context.beginPath()
            const x = Math.round(Math.random() * 600)
            const y = Math.round(Math.random() * 600)
            const size = Math.round(Math.random() * 100)
            context.fillRect(x, y, size, size)
            break
        }
    }
})
```

## Exercises

- Draw something which is:
    - Heavy
    - Fast
    - Landing
    - Slow
    - Up
    - Down
    - Opposites
    - Merging
    - Swallowing

## Putting data on canvas

...

### Resources used

- https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/getContext
- https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D
- https://egghead.io/lessons/javascript-drawing-paths-lines-and-rectangles

### TODO

- Add some info on the artists