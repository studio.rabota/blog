import { fromEvent, combineLatest, merge, of, concat, interval } from 'rxjs';
import { map, scan, takeUntil, skip, repeat, concatMap, mergeMap, flatMap, take, mergeScan, combineAll, startWith, filter, withLatestFrom, distinctUntilChanged, switchMap, first } from 'rxjs/operators';

// And send data to electronics calculator

/**
 * Canvas
 */

const canvas = document.getElementById('circuit-canvas')
const rect = canvas.getBoundingClientRect()
canvas.width = 800;
canvas.height = 800;
const context = canvas.getContext('2d')
context.lineWidth = 5
context.fillStyle = 'black'

const drawCircle = (x, y) => {
    context.beginPath()
    const radius = 5
    const startAngle = 0
    const endAngle = Math.PI * 2
    context.arc(x, y, radius, startAngle, endAngle)
    context.fill()
}

const drawCircles = ([position]) => {
    const [start, end] = position
    drawCircle(...start)
    drawCircle(...end)
}
 
const drawLine = (position) => {
    const [start, end] = position

    context.beginPath()
    context.moveTo(...start)
    context.lineTo(...end)
    context.stroke()
}

const clearCanvas = () => {
    context.clearRect(0, 0, canvas.width, canvas.height)
}

const mouseMove = fromEvent(window, 'mousemove').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

const mouseUp = fromEvent(window, 'mouseup').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

const mouseDown = fromEvent(window, 'mousedown').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

const drag = concat(
    mouseDown.pipe(first()),
    mouseMove.pipe(takeUntil(mouseUp))
)

var moves = drag.pipe(
    scan(function(acc, x) {
        return { previous: acc.current, current: x };
    }, {}),
    skip(1)
)

const example = moves.pipe(repeat());

example.subscribe(data => {
    drawLine([data.previous, data.current])
})