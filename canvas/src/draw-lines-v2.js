import { fromEvent, combineLatest, merge } from 'rxjs';
import { map, scan, takeUntil, concatMap, startWith, filter, withLatestFrom, distinctUntilChanged } from 'rxjs/operators';

// enlarge dot on hover
// Only place lines on grid
// And send data to electronics calculator

/**
 * Canvas
 */

const canvas = document.getElementById('circuit-canvas')
const rect = canvas.getBoundingClientRect()
canvas.width = 800;
canvas.height = 800;
const context = canvas.getContext('2d')
context.lineWidth = 5
context.fillStyle = 'black'

const drawCircle = (x, y) => {
    context.beginPath()
    const radius = 5
    const startAngle = 0
    const endAngle = Math.PI * 2
    context.arc(x, y, radius, startAngle, endAngle)
    context.fill()
}

const drawCircles = ([position]) => {
    const [start, end] = position
    drawCircle(...start)
    drawCircle(...end)
}
 
const drawLine = ([position, type]) => {
    const [start, end] = position

    if (type === 'r') {
        context.strokeStyle = 'blue'
    } else {
        context.strokeStyle = 'pink'
    }

    context.beginPath()
    context.moveTo(...start)
    context.lineTo(...end)
    context.stroke()
}

const clearCanvas = () => {
    context.clearRect(0, 0, canvas.width, canvas.height)
}

/**
 * Key events
 */

const anyKeyPresses = fromEvent(document, 'keypress').pipe(
    map(event => event.key)
)

const keyPressed = (key) => anyKeyPresses.pipe(filter(pressedKey => pressedKey === key))

const keyR = keyPressed('r')
const keyW = keyPressed('w')

const keys = merge(keyR, keyW).pipe(startWith('w'))

/**
 * Mouse events
 */

const mouseDown = fromEvent(window, 'mousedown').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)
const mouseMove = fromEvent(window, 'mousemove').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)
const mouseUp = fromEvent(window, 'mouseup').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

const gridPosition = ([x, y]) => {
    const grid = 20
    const column = Math.floor((x + (grid / 2)) / grid)
    const row = Math.floor((y + (grid / 2)) / grid)
    return [column * grid, row * grid]
}

const activeDot = mouseMove.pipe(
    map(gridPosition),
    distinctUntilChanged(([x, y], [xNew, yNew]) => x === xNew && y === yNew)
)

const lineDrag = mouseDown.pipe(
    withLatestFrom(activeDot),
    concatMap(([_, startPoint]) => activeDot.pipe(
        map(endPoint => [startPoint, endPoint]),
        takeUntil(mouseUp)
    ))
).pipe(
    withLatestFrom(keys)
)

const lineDraw = mouseUp.pipe(
    withLatestFrom(lineDrag),
    map(([_, line]) => line)
)

const lines = lineDraw.pipe(
    scan((lines, newLine) => [...lines, newLine], []),
)

const components = combineLatest([
    lineDrag.pipe(startWith(null)),
    lines.pipe(startWith([]))
])

components.subscribe(([lineDrag, lines]) => {
    clearCanvas()
    lines.map(drawLine)
    if (lineDrag) {
        drawCircles(lineDrag)
        drawLine(lineDrag)
    }
})