import { fromEvent, zip, concat } from 'rxjs';
import { map, scan, takeUntil, concatMap } from 'rxjs/operators';

// Draw instantly
// Able to draw resistors
// Able to connect those components

const canvas = document.getElementById('circuit-canvas')
const rect = canvas.getBoundingClientRect()
canvas.width = 800;
canvas.height = 800;
const context = canvas.getContext('2d')
context.lineWidth = 5

const drawLine = ([start, end]) => {
    context.beginPath()
    context.moveTo(...start)
    context.lineTo(...end)
    context.stroke()
}

const mouseDown = fromEvent(canvas, 'mousedown')
const mouseMove = fromEvent(window, 'mousemove')
const mouseUp = fromEvent(window, 'mouseup')

const lineStart = mouseDown.pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)
const lineMove = mouseMove.pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)
const lineEnd = mouseUp.pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

const lineDraw = zip(
    lineStart,
    lineEnd
)

const lineDrag = lineStart.pipe(
    concatMap(startPoint => lineMove.pipe(
        map(endPoint => [startPoint, endPoint]),
        takeUntil(lineEnd)
    ))
)

const lines = lineDraw.pipe(
    scan((lines, newLine) => [...lines, newLine], [])
)

// lines.subscribe(lines => lines.map(drawLine))
lineDrag.subscribe(drawLine)