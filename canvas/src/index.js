import { interval } from 'rxjs';

const { innerWidth, innerHeight } = window

const canvas = document.getElementById('canvas')
canvas.width = innerWidth;
canvas.height = innerHeight;
const context = canvas.getContext('2d')

const observable1 = interval(400);
const observable2 = interval(300);
const observable3 = interval(1000);

const drawCircle = () => {
    context.beginPath()
    const x = Math.random() * canvas.width
    const y = Math.random() * canvas.height
    const radius = Math.random() * 100
    const startAngle = 0
    const endAngle = Math.PI * 2
    context.arc(x, y, radius, startAngle, endAngle)
    context.fill()
}

const clearCanvas = () => {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

observable1.subscribe(drawCircle)
observable2.subscribe(drawCircle)
observable3.subscribe(clearCanvas)