# Goal

- Writing an circuit builder, teaching about code and electronics at the same time.

# Intro

In this book we will develop an electronics diagram builder and visualizer. Along the way you will learn TypeScript, Rxjs and electronics. Next to writing code we'll tinker with electronic components; to validate our software, and just because it's so much fun.

## TypeScript

...

## Ramda

...

## Rxjs

Life is about time. Events which have happened, are happening and are about to happen. So it is the same with software and electronics. 

Inputs are sensed via sensors and actions are decided based on these inputs. This again is the same for how humans, electronics and software operate.

Rxjs is a complex, but excellent tool for handling time. And for listening and responding to events. The concepts of Rxjs will teach you new ways of thinking about software development and it's design patterns.

## Electronics

...

# Ohm's law

- How to write Ohm's law in a function
- How to calculate current, voltage and resistance in a simple circuit

Let's start with a basic circuit. One battery (9v) and one resistor (10 Ohm).

Ohm's law states:

`V = IR`

Which can also be written, using basic algebra [link Khan academy], as: 

`I = V/R` or `R = V/I` 

The variables defined above are:
- `I` is current
- `V` is voltage
- `R` is resistance

> As you probably noticed, current is written with the conventional symbol `I`, instead of `C`. This originates from French phrase intensité du courant (current intensity) [- source](https://en.wikipedia.org/wiki/Electric_current).

## Our first function

Time to do some coding. The `I`, the `V` and the `R` are called variables in both mathematics and in software. 

In your terminal start the NodeJS REPL by writing `node` and pressing enter.

Now enter the following commands.

``` javascript
const I = undefined
const V = 9 // volt
const R = 10 // ohm
```

The values are now stored inside the variables. Type for example `R` and press enter to see the value `9` being shown as the output.

Now we want to know the current based on the voltage and resistance. We can do this with the following equation `I = V/R`.

In your terminal type:

```
> V/R
0.9
```

Great! We now calculated the current of this circuit. The overall current is `0.9` amps.

Let's make this code re-usable with a function.

``` javascript
const calculateCurrent = (V, R) => V/R
```

Let's also write functions for voltage and resistance.

``` javascript
const calculateVoltage = (I, R) => I * R
```

Can you make the one for calculating resistance yourself? The formula is `R = V/I`.

Solution:

``` javascript
const calculateResistance = (V, I) => V/I
```

We can now call the functions and fill in the params `V`, `I`,  and or `R`.

``` javascript
calculateVoltage(0.9, 10)
// => 9
```

## Ohms and kOhms

We have build a new circuit, again it contains a battery (`9v`) and a resistor. This time the resistance of the resistor is `50kOhm`.

Let's use our function `calculateResistance` to calculate the resistance `I`. 

``` javascript
const calculateResistance = (V, I) => V/I

calculateResistance(9, 50)
```

As a result we get `0.18`. Is this `amps` or `milliamps`?

When dividing a voltage with a current in `kOhms`, we get `milliamps`. When deviding a voltage with `Ohms` we get `amps` as a result.


| V       | I             | R       |
| --------|:-------------:| -----:  |
| 10      | 10 Ohms       | 1       |
| 10      | 10 kOhms      |   0.001 |
|         |               |         |

The values going in and the result coming back is unclear at the moment. We need to think about how we deal with metric prefixes. Do you have any ideas? Try to sketch out some ideas.

Off the top of the head I came up with the following solutions:
- A. Making sure only one metric unit is used for defining current
- B. Adding an extra param to the function defining that the second param is `Ohms` or `kOhms` like `calculateResistance(9, 50, "kOhms")`
- C. Writing a function which converts `kOhms` to `ohms`.
- D. Making all values able to return it's own value as each metric unit.
- ...?

Option `A` is probably not realistic, as a user you expect to set values with different metric units. Option `B` makes the conversion of `amps` to `milliamps` not re-usable. Both `C` and `D` are worth exploring. Let's go with `D`.

First we create a class called `Metric`:

```javascript
class Metric {
    #value: number;

    constructor(value: number = 0) {
        this.#value = value;
    }

    set value (value) {
        this.#value = value
    }

    get value () {
        return this.#value
    }

    set milli (value) {
        this.value = value / 1000
    }

    get milli () {
        return this.value * 1000
    }
}
```

From the base class we derive a `Resistance`, `Voltage` and `Current` class by using extend.

```javascript
class Resistance extends Metric {
    constructor(value: number = 0){
        super(value);
    }
}

class Voltage extends Metric {
    constructor(value: number = 0){
        super(value);
    }
}

class Current extends Metric {
    constructor(value: number = 0){
        super(value);
    }
}
```

We can now create clearly defined values.

```javascript
const voltage = new Voltage(9)
const current = new Current(10)
```

And calculate for example the resistance.

```javascript
const resistance = calculateResistance(voltage, current)
```

From the `resistance` variable we can now request the default value or a value converted to milli.

```javascript
console.log(resistance.value)
// 0.9 amps
console.log(resistance.milli)
// 900 milliamps
```

Because we are using TypeScript the editor can tell us which values should be passed as params. And which values are not okay. For example `calculateResistance(10, 20)` will not be allowed, we have to define it as `calculateResistance(new Voltage(10), new Current(20))`.

There's only one caveat. We can mix up the values, for example place a voltage where a current should go. TypeScript would be okay with the code below, even though Current and Voltage are placed in the wrong parameter.

```typescript
calculateResistance(new Current(20), new Voltage(10))
```

The above is possible because the shape of the classes are the same. A simple way to solve this is to make the `Metric` class generic. 

```typescript
class Metric<T> {
    unit: T
    ...
}
```

And by setting the unit like this for each subclass.

```typescript
class Resistance extends Metric<'resistance'> {
```

Now when mixing up the values, Typescript will complain. The error will show that first param in this function should be a Voltage.

```typescript
calculateResistance(new Current(20), new Voltage(10))
```

Thinking about your types makes code less error prone and easier to use for other developers. Now when using on of the functions we know where to place `Current`, `Resistance` and `Voltage`. We also know that each value is without a metric prefix such as `kiloohms` or `milliamp`, while still being able to request the value with a metric prefix using the method `milli`. 

## Functional programming

There are two main styles of programming. Object Orientend Programming (OOP) and functional programming. The `Metric` class and it's subclasses `Voltage`, `Current` and `Resistance` are examples of OOP.

The class created in the previous chapter is pretty neat. However I prefer a functional programming style for most applications written in Javascript. Functional programming tends to be more declarative and less verbose. In this chapter we'll use Ramda to make functional programming easier.

The first rule of functional programming we'll adhere to is one input and one output for each function. Functional programming is based on mathematics, and this rule, as many other rules are defined in mathematics. [^1] [^2]

``` javascript
// incorrect
const someFunction = (argument1, argument2) => {}
// correct
const someFunction = (argument1) => {}
```

Our current `calculateResistance` function however takes two arguments. How can we fix this to only accept one argument? 

``` javascript
const calculateResistance = (V, I) => V/I
```

We can adhere to the rule of one input and one output by creating a curry function. A curry is nothing more then a function which returns a function.

``` javascript
const calculateResistance = (V) => (I) => V/I
```

We can now call the function like:

``` javascript
const resistance = calculateResistance(9)(50)
```

Or we can use the function like:

``` javascript
const calculateResistance9v = calculateResistance(9)
const resistance = calculateResistance9v(50)
```

Ramda provides the `curry` utility to make curried functions.

``` javascript
import R from 'ramda'
const calculateResistance = R.curry((V, I) => V/I)

const resistance = calculateResistance(9)(50)
```

Using Ramda's `curry` has the added benefit that you can still call `calculateResistance` without the need of doubling up on brackets.

``` javascript
const resistance = calculateResistance(9)(50)
const resistance = calculateResistance(9, 50)
```

Another good rule of thumb is to never ever have to define params. At the moment we're still defining `V` and `I`. 

> Names are a bit of an issue, you see. We have potential misnomers - especially as the codebase ages and requirements change. - From Dr Boolean, the [mostly-adequate-guide](https://mostly-adequate.gitbooks.io/mostly-adequate-guide/content/ch02.html)

``` javascript
const calculateResistance = R.curry((V, I) => V/I)
```

Because we apply the rule of one input, one output we can now do something powerful. We can make a general curry function like:

``` javascript
const divide = R.curry((a, b) => a/b)
const calculateResistance = divide
```

However, no need for this, as Ramda already provides such a function.

``` javascript
const calculateResistance = R.divide
const resistance = calculateResistance(9, 50)
```

- [^1] https://drboolean.gitbooks.io/mostly-adequate-guide-old/content/ch3.html#8th-grade-math
- [^2] https://www.mathsisfun.com/sets/function.html

## Semigroups

In the previous chapter we kind of skipped over the `R.divide`. In Ramda this is basically as simple as:

``` javascript
var divide = _curry2(function divide(a, b) { return a / b; });
```

Now there is a another way of doing a mathematical operation in functional programming by using a type called a Semigroup.

> In mathematics, a semigroup is an algebraic structure consisting of a set together with an associative binary operation. - [Wiki](https://en.wikipedia.org/wiki/Semigroup)

The definition of a Semigroup is that it needs to have a concat method. It's type signature is as such that one variable `a` is combined with another variable of the same type `a` to create an output of the same type `a`. The type of `a` can be for example a `Number`, `String`, array of `String`, etc. [^1]

In native Javscript we can already concat a `String`, like:

``` javascript
const ab = 'a'.concat('b')
```

However for a `Number` this is not possible.

``` javascript
const error = 1.concat(2)
```

We can howewer make our own containers, for example:

```javascript
const Sum = x => ({
    x,
    concat: ({x: y}) => Sum(x + y)
})

const Divide = x => ({
    x,
    concat: ({x: y}) => Divide(x / y)
})
```

We can now divide our values as:

```javascript
const calculateCurrent = (voltage, current) => Divide(voltage).concat(Divide(current))

const current = calculateCurrent(9, 50)
```

And do additions like:

```javascript
const two = Sum(1).concat(Sum(1))
```

Semigroups also need to have another property called associativity. This means that the grouping of operations does not matter on the final result. [^2] The following are some examples of this property.

> In mathematics, an associative operation is a calculation that gives the same result regardless of the way the numbers are grouped. - [source](https://www.computerhope.com/jargon/a/assooper.htm) 

As a math sum:
``` javascript
(1 + 1) + 1 === 1 + (1 + 1)
```

When combining strings:
``` javascript
'a'.concat('b').concat('c') === 'a'.concat('b'.concat('c'))
```

This also holds for our `Sum` container.
``` javascript
Sum(1).concat(Sum(1).concat(Sum(1))) // 3
=== 
Sum(1).concat(Sum(1).concat(Sum(1))) // 3
```

However not for `Divide`

> Addition and multiplication are both associative, while subtraction and division are not. - [source](https://www.computerhope.com/jargon/a/assooper.htm) 

``` javascript
Divide(12).concat(Divide(3)).concat(Divide(2)) // 2
!== 
Divide(12).concat(Divide(3).concat(Divide(2))) // 8
```

- [^1] [Fantasy Land](https://github.com/fantasyland/fantasy-land#semigroup)
- [^2] [Egghead](https://egghead.io/lessons/javascript-combining-things-with-semigroups

## Watts (Power)
 
To calculate Watts: `VI = W`. In this example we have 12 volts and 2 amps.

// TODO: Add description on what watts is

```javascript
const voltage = 12 
const current = 2 // amps 
const calculateWatts = R.multiply
const watts = calculateWatts(voltage, current)
```

We can also calculate current based on voltage and watts, `I = W/V`.

```javascript
const voltage = 12 
const watts = 24 
const calculateCurrent = R.divide
const current = calculateCurrent(watts, voltage)
```

Or calculate voltage based on current and watts, `V = W/I`.

```javascript
const watts = 24 
const current = 2 // amps 
const calculateVoltage = R.divide
const voltage = calculateVoltage(watts, voltage)
```

## Series resistors (circuit analysis)

Aaaalllright, so we now know how to calculate a simple circuit with one resistor. How about when we have multiple resistors in series? 

![Series resistor](./img/Resistors_in_series.svg)

> By Omegatron - This W3C-unspecified circuit diagram was created with the Electrical Symbols Library, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=2027420

It's pretty easy actuallly. You just have to sum up the resistance of all resistors. Let's say that `R1` and `R2` are both 100 ohms. We also know the voltage to be `9v`. Let's calculate the current, `I = V/R`.

``` javascript
const voltage = 9
const R1 = 100 // Ohms
const R2 = 100 // Ohms
const calculateCurrent = R.divide
const current = calculateCurrent(9, R1 + R2)
```

This works fine for two resistors, however when we have for some odd reason 10 resistors in series, the code becomes somewhat clunky. Let's make a list of values. 

``` javascript
const voltage = 9
const resistors = [100, 100, 47]
const calculateCurrent = R.divide
const calculateSeriesResistance = R.sum

const current = calculateCurrent(
    voltage, 
    calculateSeriesResistance(resistors)
)
```

We could also decide to make the second argument to always be a list of values.

``` javascript
const voltage = 9
const resistors = [100, 100, 47]
const calculateSeriesResistance = R.useWith(
    R.divide,
    [R.identity, R.sum]
)

const resistance = calculateSeriesResistance(voltage, resistors)
```

Wow! Let's break it down. 

`R.useWith` accepts a function as it's first argument and a list of transformer functions as the second argument. We use `R.sum` to get the sum of `100`, `100`, and `47`. And use `R.identity` since voltage does not have to be transformed. 

`R.identity` is simply a function which returns it's own value. In functional lingo this is called an `Identity Functor`.

``` javascript
const id = (x) => x
``` 

In the calculation above we used values from resistors. Resistance can also come from other components such as a light source or a motor. The calculation will stay the same.

## Parallel resistors (circuit analysis)

Aaaalllright, let's continue our circuit analaysis. We now know how to calculate voltage, current and resistance for a simple circuit. Let's make the circuit a bit more complicated.

In this circuit the resistors are placed in parallel. Let's say we have two resistors `R1` (`47 ohms`) and `R2` (`100 ohms`) placed in parallel. We also know the voltage to be `9v`.

![Series resistor](./img/Resistors_in_parallel.svg)

> By Omegatron, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons

When there's two resistors in parallel you can use the following equation `(R1 x R2) / (R1 + R2)`. So in this case `(47 x 100) / (47 + 100)` is `32 ohms`.

Let's do that one in code.

``` javascript
const R1 = 47
const R2 = 100

const calculateParallelResistance = (R1, R2) => (R1 x R2) / (R1 + R2)

const resistance = calculateParallelResistance(R1, R2)
```

Let's make that one a bit more functional. 

``` javascript
const R1 = 47
const R2 = 100

const calculateParallelResistance = R.useWith(
    R.divide,
    [R.product, R.sum]
)

const resistance = calculateParallelResistance([R1, R2], [R1, R2])
```

Great! That works, however adding the same values twice is slightly awkward. Can we make it `calculateParallelResistance([R1, R2])`. Yes we can! Simply change `R.useWith` to `R.converge`.

``` javascript
const R1 = 47
const R2 = 100

const calculateParallelResistance = R.converge(
    R.divide,
    [R.product, R.sum]
)

const resistance = calculateParallelResistance([R1, R2])
```

> Fun fact! When the resistors ar the same value, you can just divide the value by `2`. For example `(100 x 100) / (100 + 100) = 50 = 100 / 2`

We're not done here though. When we have more then two resistors in parallel the formula becomes a tad bit more complicated. 

```
(1/RT) = (1/R1) + (1/R2) + (1/R3) + ... + (1/Rn)
```

In the equation above `RT` is the total of `R1`, `R2`, `R3`, etc. Let's say `R1` and `R2` is `100` and `R3` is `47`.

```
RT = (1/100) + (1/100) + (1/47) = 0.04
(1/RT) = (1/0.04) = 24
```

Let's see how our previous function deals with this. 

``` javascript
const resistance = calculateParallelResistance([100, 100, 47])
console.log(resistance) // 1902.834008097166
```

Aih `1902.834008097166` is indeed not correct. New specs, new function, here we go! This new function works for two and more resistors in parallel. 

``` javascript
const R1 = 100
const R2 = 100
const R3 = 47

const divide1By = R.divide(1)

const calculateParallelResistance = R.compose(
    divide1By, 
    R.sum, 
    R.map(divide1By)
)

const resistance = calculateParallelResistance([R1, R2, R3])
```

Woop woop! Two new utility functions, `map` and `compose`.

Compose is a function which passes data trough the functions set in the arguments. The order is from right to left. 

1. `divide1By` is called on each number 
3. `R.sum` spits out a total of the numbers from step `1`
4. `divide1By` divides `1` by the total of step `3`. 

We could have written the same by calling each function in the same order, like: 

``` javascript
const calculateParallelResistance = (resistors) => divide1By(R.sum(R.map(divide1By, resistors)))
``` 

We use compose `R.compose` instead of `(resistors) => divide1By(R.sum(R.map(divide1By, resistors)))` because of readability. There's only so much parenthesis a developer's mind can take.

> Compose is based on mathematics (category theory). 

So how about `R.map`. It's super simpel and suuuper powerful. The first argument takes in a function, and the second argument takes in our functor (array) `[R1, R2, R3]`. The function from the first argument `divide1By` is applied to all values in the functor (array).

Hold up! functor?! `functor` is a fancy name from category theory, and is simply said a container which holds a value and implements a `map` method. [^1] Also a `functor` has to return the same type, another `functor`. In this case `[100, 100, 47]` becomes `[0.1, 0.1, 0.02127659574]`.

- [^1] https://mostly-adequate.gitbooks.io/mostly-adequate-guide/content/ch08.html#my-first-functor

## Adding TypeScript to our functional code

In previous chapter we noticed that it's hard to follow which metric unit is returned. We solved this by containing our values in classes.

We also added typing with Typescript so that values will not be placed into the wrong arguments, preventing possible errors along the way. How would we do this while using a functional programming style?

// TODO: write example with typing

# Kirchhoff's Laws (circuit analysis)

> The sum of all the voltages around a loop is equal to zero. [3]

Aaaallllrighty then, let's implement the first of Kirchhoff's Laws, Kirchhoff's Current Law a.k.a KCL for the cool kids. 

![Kirchhoff's Current Law](./img/KCL_-_Kirchhoff's_circuit_laws.svg)

> KCL.png: Pflododerivative work: M0tty, CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>, via Wikimedia Commons

The law states that the current going into a node must be equal to the current coing out. In the diagram above this would mean `i2 + i3 = i1 + i4`.

The sum of `i` going in is equal the the sum of `i` going out (`Σi-in = Σi-out`).

The sum of `i` on a node going in is equal to zero (`Σi = 0`). When there's a node with 4 lines attached and 3 of those lines have 1A going into the node, then the 4th line is -3A (`1A + 1A + 1A + -3A = 0`).  [1]

Let's make a simple function to check this.

``` javascript
const nodeCurrents = [1, 1, 1, -3]

const KCL = R.compose(R.equals(0), R.sum)

const result = KCL(nodeCurrents) // true
```

Let's also make simple function to calculate the current going out based on the currents going in. Let's say we have a node with 4 wires, we know the values of 3 wires to be 1A.

``` javascript
const currentsIn = [1, 1, 1]

const currentOut = R.compose(R.multiply(-1), R.sum)

const result = currentOut(currentsIn) // -3
```

Next! We'll now implement Kirchhoff's Voltage Law a.k.a KVL. KVL states that `Σv-rise - Σv-fall = 0`. Let's say we have a voltage supply of `10` volts and two resistors of both `100` ohms in series. The rise of the voltage is `10` volts, the drop over each resistor is `5` volts (`10 - (2 * 5) = 0`). [2]

``` javascript
const voltageRise = [10]
const voltageDrop = [5, 5]

const sumAndSubtract = R.useWith(
    R.subtract,
    [R.sum, R.sum]
)

const KVL = R.compose(R.equals(0), sumAndSubtract)

const result = KVL(voltageRise, voltageDrop) // true
```

![Kirchhoff's Voltage Law](./img/Kirchhoff_voltage_law.svg)

> Kwinkunks, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons

Another shorthand form to write KVL in is, `Σv = 0`. 

It works like this. Pick a point in the diagram above and start moving to the next point, make a list of the voltage rises and drops. Let's say we start at `d` and move to `a`, we have a rise of `10` volts. From `a` to `b` we have a drop of `-2` volts, `b` to `c` is `-3` volts and `c` to `d` is `-5` volts. Then `Σv = 0` would be `10 - 2 - 3 - 5 = 0`.

Let's put this into code as well.

``` javascript
const DA = 10
const AB = -2
const BC = -3
const CD = -5

const KVL = R.compose(R.equals(0), R.sum)

const result = KVL([DA, AB, BC, CD]) // true
```

The point where you start does not matter.

```javascript
const result = KVL([DA, AB, BC, CD]) === KVL([BC, CD, DA, AB]) // true
```

- [1] https://www.khanacademy.org/science/ap-physics-1/ap-circuits-topic/kirchhoffs-junction-rule/v/ee-kirchhoffs-current-law
- [2] https://www.khanacademy.org/science/ap-physics-1/ap-circuits-topic/kirchhoffs-loop-rule-ap/v/ee-kirchhoffs-voltage-law?modal=1
- [3] https://en.wikipedia.org/wiki/Kirchhoff's_circuit_laws#Kirchhoff's_voltage_law

# Capacitors

Capacitor values are measured in Farads. You will usually come accross `micro` or `nano` Farads since the values are generally small.

> The farad (symbol: F) is the SI derived unit of electrical capacitance, the ability of a body to store an electrical charge. It is named after the English physicist Michael Faraday. - https://en.wikipedia.org/wiki/Farad

## Parallel

Let's take a simple circuit with two capacitors in parallel. How would we calculate the capicitance?

![Capacitors in parallel](./img/Capacitors_in_parallel.svg)

> Omegatron, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons

The calculation for resistors in series is the same as capacitators hooked up in parallel. And the calulation for resistors in parallel is the same as capacitators in series. Funny no! // TODO: Little drawing

Let's say we have a capacitator of `10pf` and `22pF` (pico Farad).

When capacitors are hooked up in parallel, you can just sum up the values of each capacitor. 

``` javascript
const capacitance = [10, 22]
const calculateParallelCapacitance = R.sum

const totalCapacitance = calculateParallelCapacitance(capacitance)
```

## Series

How about a simple circuit with two capacitors in series, `C1` is `10pF` and `C2` is `22pf`.

![Capacitors in series](./img/Capacitors_in_series.svg)

> Omegatron, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons

When there's two capicitators in series you can use the following equation `(C1 x C2) / (C1 + C2)`. So in this case `(10 x 22) / (10 + 22)` is `6.875pF`.

Let's do that one in code.

``` javascript
const C1 = 10
const C2 = 22

const calculateSeriesCapacitance = (C1, C2) => (C1 x C2) / (C1 + C2)

const totalCapacitance = calculateSeriesCapacitance(C1, C2)
```

And let's make the fucntion above a bit more functional. 

``` javascript
const C1 = 10
const C2 = 22

const calculateSeriesCapicitance = R.converge(
    R.divide,
    [R.product, R.sum]
)

const totalCapicitance = calculateSeriesCapicitance([C1, C2])
```

> Fun fact! When the capacitors ar the same value, you can just divide the value by `2`. For example `(22 x 22) / (22 + 22) = 11 = 22 / 2`

We're not done here though. The function above only works for two capacitors in parallel. When we have more then two capacitors in parallel the formula becomes a tad bit more complicated. 

```
(1/Ct) = (1/C1) + (1/C2) + (1/C3) + ... + (1/Cn)
```

In the equation above `Ct` is the total of `1/C1`, `1/C2`, `1/C3`, etc. Let's say `C1` and `C2` is `10pF` and `C2` is `22pF`.

```
Ct = (1/10) + (1/10) + (1/22) = 0.24545454545454548 pF
(1/Ct) = (1/0.24545454545454548) = 4.0740740740740735 pF
```

This new function works for two and more capacitors in parallel. 

``` javascript
const C1 = 10
const C2 = 10
const C3 = 22

const divide1By = R.divide(1)

const calculateSeriesCapacitance = R.compose(
    divide1By, 
    R.sum, 
    R.map(divide1By)
)

const capacitance = calculateSeriesCapacitance([C1, C2, C3])
```

# Diode

![Diode](./img/Diode_pinout_en_fr.svg)

> Erik Streb, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons

A diode consist of two pieces of silicon joined together. One side is called `N` (n-type) with an access of electrons and the other `P` (p-type) with a lack of electrons (electron holes). 

In the center of the diode you have the depletion region, in which the electrons balance out (no excess or shortage of electrons). The depletion region has a very high resistance.

## Reversed Bias

When you take a battery and hook up the positive side to the `Cathode` and the negative to the `Anode` the depletion region increases and the resistance becomes even higher. This is called Reversed Bias.

## Forward Bias

When you take a battery and hook up the negative side to the `Cathode` and the positive to the `Anode` the depletion region becomes smaller and the resistance gets lower. Eventually, with enough voltage, the depletion region becomes small enough to let current trough. This is called Forward Bias.

The voltage which needs to be applied for a Silicon diode is around `0.7 volts`. For Germanium diodes it is around `0.3` volts.

## Always add a resistor

When forward biased is achieved the resistance becomes non existent. This means that when you take a power supply with a voltage of `3` volts, the current becomes `Infinity`. Just do the maths `I = V / R`, means `I = 3 / 0  = Infinity`. Waaaaat. In reality this means the battery will put out it's max current and overheat (burn out) the diode. No good.

You therefore always have to add a resistor to limit the current. Let's add a `1K` resistor. The voltage drop over the diode is `0.7` volts. Meaning there's `2.3` volts over the resistor. `I = 2.3 / 1000 (1K) = 0.0023 A`. We now have `0.0023 A` of current, much better. Happy diode.

## Brigde rectifier

Oki doki, let's make an implementation of a bridge rectifier.

A bridge rectifier can be used to convert an AC signal to a DC signal. It can also provide protection for when for ex. batteries are installed backwards [^1]

To make a bridge rectifier you need to connect 4 diodes similar to the following diagram.

![Diode Bridge](./img/Diode_bridge_alt_1.svg)

> User:Wykis, Public domain, via Wikimedia Commons

Notice how the plus and minus do not alternate like an AC signal. The connections after the diode bridge are always the same polarity.

![Diode Bridge](./img/Diode_bridge_alt_2.svg)

> User:Wykis, Public domain, via Wikimedia Commons

- [^1] https://en.wikipedia.org/wiki/Diode_bridge

Let's write the modal of a Sillicon diode in code. Here's the specs:

1. Negative on the `Cathode` and positive on the `Anode` creates forward bias when the voltage is over `0.3` volts.
2. Positive on the `Cathode` and negative on the `Anode` creates reversed bias.

First we'll create a boundary of `0.7` volts.

```js
const forward = R.lt(0.7)
```

```js
const cathodeToAnode = () => {

}
const anodeToCathode = () => {
    
}
```

Next we'll need a voltage source which keeps flipping it's values. We'll use RXJS for this.

```js
```

# Nodal analysis

To calculate the voltages we need to use nodal analysis. See Khan academy for explaining the [node voltage method](https://www.khanacademy.org/science/electrical-engineering/ee-circuit-analysis-topic/ee-dc-circuit-analysis/v/ee-node-voltage-method-steps-1-to-4) method. 

> "Kirchhoff's current law is the basis of nodal analysis." - [source](https://en.wikipedia.org/wiki/Nodal_analysis)

![Kirchhoff's Current Law](./img/Nodal_analysis.svg)

> Petteri Aimonen, Public domain, via Wikimedia Commons

To apply the node voltage method we have to follow a sequence of steps.

These steps are [^1]: 
1. Pick a reference node
2. Name the node voltages
3. Solve the easy nodes
4. Write KCL equations
5. Solve the equations

## Step 1

In the diagram above the symbol with 3 stripes at the bottom is a reference node. 

A good place to set the reference node is [^1]: 
- Connected to the power source(s)
- Or a node that is connected to a lot of branches

## Step 2

Name the node voltages. In the example above we have `V1`. The node voltage is measued between the node and the reference node [^1]. In the diagram this is for example between `V1` an the reference node.

## Step 3

Solve the easy nodes. The diagram above does not show this, but we could have placed a node at the top left corner. This node would be easy to solve since it's connected to the power source and would therefore be `5v`.

## Step 4

Write KCL equations. For `V1` the equation would be, `+i1 + Is - i2 = 0`. 
- `i1` = `(Vs - V1) / R1`
- `i2` = `V1 / R2`

The equation for `V1` is `(Vs - V1) / R1 + Is - V1 / R2`.

Notice how the equations above uses simple Ohm's law, only with the node voltages.

## Step 5

Solve the equations. Let's replace the variables with values.

`(5v / 100) - (V1 / 100) + 20mA - (V1 / 200) = 0`

We move `Is` to the other side and convert from milli to ampere.   

`(5v / 100) - (V1 / 100) - (V1 / 200) = -0.02A`

We extract `V1`.

`V1 * (-1/100 - 1/200) = -0.02A - (5v / 100)`

Simplify.

`V1 * (-3/200) = -0.02A - 0.05mA`

And keep only `V1` on the left.

`V1 = -0.07A * (-200/3)`

Result

`V1 = -4,6666666667V`

This is how we can solve the node voltages manually. Next step is to convert this into code.  

Luckily we have [this](http://www.ecircuitcenter.com/SpiceTopics/Nodal%20Analysis/Nodal%20Analysis.htm) great article explaining how voltages are calculated in [Spice](https://en.wikipedia.org/wiki/SPICE#Commercial_versions_and_spinoffs). We can now build a way to calculate the voltages.

In Spice the circuit is written as a Netlist, see info on [Wiki](https://en.wikipedia.org/wiki/Netlist) and [AllAboutCircuits](https://www.allaboutcircuits.com/textbook/reference/chpt-7/example-circuits-and-netlists/)

```
LINEAR_DC_CKT.CIR - SIMPLE CIRCUIT FOR NODAL ANALYSIS
*
IS	0	1	DC	1A
*
R1	1	0	10
R2	1	2	1K
R3	2	0	1K
*
* ANALYSIS
.TRAN 	1MS  10MS
* VIEW RESULTS
.PRINT	TRAN 	V(1) V(2)
.PROBE
.END
```
> Example of a Netlist taken from http://www.ecircuitcenter.com/SpiceTopics/Nodal%20Analysis/Nodal%20Analysis.htm

Well take the circuit from the `ecircuitcenter.com` to write our first implementation. TODO: Add diagram image

We can represent the graph data in code by making it a adjacency list.

``` js
const adjacencyList = {
    0: [1],
    1: [0, 2],
    2: [0]
}
```

Or an adjaceny matrix. 

``` js 
const adjacencyMatrix = [
    [0, 1, 0],
    [1, 0, 1],
    [1, 0, 0]
]
```

<!-- Notice how in the matrix above we do not include the power source.  -->
<!-- This will come back later. -->

However, because we need to know direction a directed graph is better.

``` js 
const adjacencyMatrix = [
    [0, 1, 0], // node 1
    [1, 0, 1], // node 2
    [1, 0, 0] // node 3
]
```

Let's plug in the values instead of 0 or 1.

``` js
// const adjacencyMatrix = [
//     [0, 0, 0], // Power source
//     [10, 0, 1000],
//     [1000, 0, 0]
// ]

const simpleMatrix = matrix([
    // 0 | 1 | 2
    [0, '1A', 0], // 0 source
    [-10, 0, -1000], // 1 v1
    [-1000, 1000, 0] // 2 v2
])
```

We'll go for the adjacency matrix. This adjacency matrix will be converted to a matrix with the following equations.

``` js
const G = matrix([
    [1/R1.resistance + 1/R2.resistance,  -(1/R2.resistance)],
    [-(1/R2.resistance), 1/R2.resistance + 1/R3.resistance]
])
```

- [^1] https://www.khanacademy.org/science/electrical-engineering/ee-circuit-analysis-topic/ee-dc-circuit-analysis/v/ee-node-voltage-method-steps-1-to-4

# Making a signal output with RXJS.

Aaaallllright let's make an electric signal. A fake one that is..

// TODO: This needs to be checked
So first we're going to make a vector rotation. We'll be using the same formulas to make the vector rotation as we use to analyse signals. 

Javascript actually has some build in variables.
- Math.E is the Euler's constant
- Math.cos()
- Math.sin()
- and many more https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/cos

# Schematic drawing (drawing circuits)

OOOOkay dan, let's make a program to draw our electronic circuit. We'll be using the `<canvas />` element for this and some Javascript. Let's first draw a grid. 



# Printing the schematic with a 3d printer and adding the components

# On units

In the SI system of units there are 7 base units. - [source](https://en.wiktionary.org/wiki/derived_unit)

- length (meter)
- mass (kilogram)
- time (second)
- electric current (ampere) 
- thermodynamic temperature (kelvin) 
- amount of substance (mole) 
- luminous intensity (candela)

Ohms is a derived unit from length, mass, time, and electric current [^1]. 

The kilo (k) from  `kiloohms` or `kOhms` is a metric prefix with the power 10-3. Meaning 1 Ohm is 0.001 kOhm. And 1 kOhm is 1000 Ohm.

There are many more metric prefixes suchs as milli (m) 	10-3, micro (µ) 10-6, nano (n) 	10-9, pico (p) 	10-12. [^2]

For ampere we often use `amp` or `milliamp`. The metric prefix milli (m) is 10-3 [^3], meaning 1 amp is 1000 milliamps and 1 milliamp is 0.001 amp 

Farads, used for capacitors is usually in `micro` or `nano`.

Let's make a simple converter to convert from one metric prefix to another. Below is a simplified table, a full table can be found at [Wikipedia](https://en.wikipedia.org/wiki/Metric_prefix#Energy) [^4].

| Name          | Symbol| Decimal  |
| ------------- |-------| -----:   |
| tera          | T     | 1 000 000 000 000  
| giga          | G     | 1 000 000 000 
| mega          | M     | 1 000 000 
| kilo          | k     | 1 000
| hecto         | h     | 100
| deca          | da    | 10     
|               |       | 1
| deci          | d     | 0.1
| centi         | c     | 0.01
| milli         | m     | 0.001
| micro         | μ     | 0.000 001 
| nano          | n     | 0.000 000 001 
| pico          | p     | 0.000 000 000 001

Let's place this data into an object. We'll use the exponential notation to keep it short and sweet [^5].

> You can use `Number.toFixed` for converting exponential notation to a full notation `Number.parseFloat(-1e4).toFixed()`. And `Number.toExponential` for the other way around `Number.parseFloat(0.001).toExponential()`.

``` javascript
{
    T: {
        decimal: 1e-12,
        name: "tera"
    },
    p: {
        decimal: -1e-12,
        name: "pico"
    }
}
```

// TODO: Add function to convert values

- [^1] https://en.wiktionary.org/wiki/derived_unit
- [^2] https://learn.sparkfun.com/tutorials/metric-prefixes-and-si-units/all
- [^3] https://www.nist.gov/pml/weights-and-measures/metric-si-prefixes
- [^4] https://en.wikipedia.org/wiki/Metric_prefix#Energy
- [^5] https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toExponential