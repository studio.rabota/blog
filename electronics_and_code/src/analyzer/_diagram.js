import * as R from 'ramda'
import { calculateNodes } from './_matrix.js'

const diagram = [
    {
        position: [{x: 20, y: 300}, {x: 20, y: 10}],
        data: { id: 'IS', weight: '1', type: 'current-source' }
    }, 
    {
        position: [{x: 20, y: 10}, {x: 200, y: 10}],
        data: { id: 'line1', type: 'wire' }
    },
    {
        position: [{x: 200, y: 10}, {x: 200, y: 300}],
        data: { id: 'R1', weight: '10', type: 'resistor' }
    },
    {
        position: [{x: 200, y: 10}, {x: 600, y: 10}],
        data: { id: 'R2', weight: '1000', type: 'resistor' }
    },
    {
        position: [{x: 600, y: 10}, {x: 600, y: 300}],
        data: { id: 'R3', weight: '1000', type: 'resistor' }
    },
    {
        position: [{x: 600, y: 300}, {x: 200, y: 300}],
        data: { id: 'line2', type: 'wire' }
    },
    {
        position: [{x: 200, y: 300}, {x: 20, y: 300}],
        data: { id: 'line3', type: 'wire' }
    }
]

const positions = diagram.flatMap(({position}) => position)
const nodes = R.uniq(positions)

const edges = diagram.map(({data, position}) => {
    const [start, end] = position
    const source = nodes.findIndex(R.equals(start))
    const target = nodes.findIndex(R.equals(end))

    return {
        ...data,
        source,
        target
    }
})

const componentEdges = edges.filter(({type}) => type !== 'wire')

const findComponentSource = (edges) => (edge) => {
    if (edge.type !== 'wire') {
        return edge.source
    }

    const connectedEdges = findEdges(edges)(edge)
    return R.uniq(connectedEdges.flatMap(findComponentSource(edges)))
}

const findEdges = (edges) => (edge) => edges.filter(data => data.source === edge.target)

const edgesWithoutWires = componentEdges.flatMap((edge) => {
    const connectedEdges = findEdges(edges)(edge)
    const connectedComponents = connectedEdges.flatMap(edge => findComponentSource(edges)(edge))

    return connectedComponents.map(target => {
        return {
            ...edge,
            target
        }
    })
})

const parseDiagram = (diagramComponents) => {
    const count = {
        'V': 0, // voltage
        'I': 0, // current
        'R': 0, // resistor
        'C': 0, // capacitor
        'L': 0, // inductor
        ...R.countBy(R.head)(diagramComponents.map(component => component.id))
    }

    const components = diagramComponents.map(component => {
        return {
            comp_type: R.head(component.id), 
            high_str: component.source.toString(), 
            low_str: component.target.toString(),
            value: parseFloat(component.weight),
            high: -1,
            low: -1
        }
    })

    return {
        count,
        components
    }
}

const { components, count } = parseDiagram(edgesWithoutWires)
const { result } = calculateNodes(components, count)

console.log('result', result)