import * as R from 'ramda'
import { inv, multiply, zeros, subset, index } from 'mathjs'

export const mapNodes = (components) => {
    const highStrings = components.map(component => component.high_str)
    const lowStrings = components.map(component => component.low_str)
    const keys = R.uniq(['0', ...highStrings, ...lowStrings])
    const table = keys.reduce((accumulator, currentValue, index) => ({...accumulator, ...{[currentValue]: index}}), {});

    const tableComponents = components.map(component => ({
        ...component,
        high: table[component.high_str],
        low: table[component.low_str]
    }))

    return {
        tableComponents,
        table,
        nodeCount: keys.length
    }
}

export const calculateMatrices = (components, count, nodeCount) => {
    const g2Count = count.V + count.L
    const matrixSize = nodeCount + g2Count - 1

    let A = zeros(matrixSize, matrixSize)
    let b = zeros(matrixSize)

    // Group 2 component index
    let g2Index = matrixSize - g2Count

    components.forEach(component => {
        const { high, low, value } = component

        if (component.comp_type == 'R') {
            // affects G-matrix of A
            // diagonal self-conductance of node
            if (high != 0) {
                A = subset(
                    A, 
                    index(high - 1, high - 1), 
                    subset(A, index(high - 1, high - 1)) + 1/value
                )
            }
            if (low != 0) {
                A = subset(
                    A, 
                    index(low - 1, low - 1), 
                    subset(A, index(low - 1, low - 1)) + 1/value
                )
            }

            // mutual conductance between nodes
            if (high != 0 && low != 0) {
                A = subset(
                    A, 
                    index(high - 1, low - 1), 
                    subset(A, index(high - 1, low - 1)) - 1/value
                )
                A = subset(
                    A, 
                    index(low - 1, high - 1), 
                    subset(A, index(low - 1, high - 1)) - 1/value
                )
            }
        } 

        else if (component.comp_type == 'L') {
            // closed circuit  in Static Analysis: 0 resistance and 0 voltage
            // affects the B and C matrices of A
            if (high != 0) {
                A = subset(
                    A, 
                    index(high - 1, g2Index), 
                    subset(A, index(high - 1, g2Index)) + 1
                )
                A = subset(
                    A, 
                    index(g2Index, high - 1), 
                    subset(A, index(g2Index, high - 1)) + 1
                )
            }
            if (low != 0) {
                A = subset(
                    A, 
                    index(low - 1, g2Index), 
                    subset(A, index(low - 1, g2Index)) - 1
                )
                A = subset(
                    A, 
                    index(g2Index, low - 1), 
                    subset(A, index(g2Index, low - 1)) - 1
                )
            }

            // affects b-matrix
            b = subset(
                b, 
                index(g2Index), 
                0
            )

            g2Index = g2Index + 1
        }

        // elif component.comp_type == 'C':
            // Capacitance is an open circuit for Static Analysis

        else if (component.comp_type == 'V') {
            // affects the B and C matrices of A
            if (high != 0) {
                A = subset(
                    A, 
                    index(high - 1, g2Index), 
                    subset(A, index(high - 1, g2Index)) + 1
                )
                A = subset(
                    A, 
                    index(g2Index, high - 1), 
                    subset(A, index(g2Index, high - 1)) + 1
                )
            }
            if (low != 0) {
                A = subset(
                    A, 
                    index(low - 1, g2Index), 
                    subset(A, index(low - 1, g2Index)) - 1
                )
                A = subset(
                    A, 
                    index(g2Index, low - 1), 
                    subset(A, index(g2Index, low - 1)) - 1
                )
            }

            // affects b-matrix
            b = subset(
                b, 
                index(g2Index), 
                value
            )

            g2Index = g2Index + 1
        }

        else if (component.comp_type == 'I') {
            // affects b-matrix
            if (high != 0) {
                b = subset(
                    b, 
                    index(high - 1), 
                    subset(b, index(high - 1)) - value
                )
            }
            if (low != 0) {
                b = subset(
                    b, 
                    index(low - 1), 
                    subset(b, index(low - 1)) + value
                )
            }
        }
    })

    return { A, b }
}

export const calculateNodes = (components, count) => {
    const { tableComponents, table, nodeCount } = mapNodes(components)

    const { A, b } = calculateMatrices(tableComponents, count, nodeCount)

    const invA = inv(A)
    const result = multiply(invA, b)

    return {
        result,
        count,
        table
    }
}