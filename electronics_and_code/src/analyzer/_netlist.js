// https://github.com/drifter1/circuitsim

import fs from 'fs'
import R from 'ramda'
import { calculateNodes } from './_matrix.js'

const content = fs.readFileSync("example2.spice", { encoding: 'utf8' });

const parseFile = (content) => {
    const lines = content.split("\n")

    const count = {
        'V': 0, // voltage
        'I': 0, // current
        'R': 0, // resistor
        'C': 0, // capacitor
        'L': 0, // inductor
        ...R.countBy(R.head)(lines)
    }

    const components = lines.map(line => {
        const lineParts = line.split(" ")
        return {
            comp_type: R.head(lineParts[0]), 
            high_str: lineParts[1], 
            low_str: lineParts[2],
            value: parseFloat(lineParts[3]),
            high: -1,
            low: -1
        }
    })

    return {
        count,
        components
    }
}

const { components, count } = parseFile(content)
const { result } = calculateNodes(components, count)

console.log('result', result)