import { fromEvent, merge, of, BehaviorSubject } from 'rxjs';
import { map, takeUntil, skipWhile, repeat, flatMap, withLatestFrom, switchMap } from 'rxjs/operators';
import { sort, allPass } from 'ramda'
import { drawLine, clearCanvas, rect } from './canvas'

/**
 * Mouse
 */

const mouseMove = fromEvent(window, 'mousemove').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

const mouseUp = fromEvent(window, 'mouseup').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

const mouseDown = fromEvent(window, 'mousedown').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

/**
 * Lines
 */

const lines = new BehaviorSubject([
    {
        id: 0,
        position: [[340, 580], [500, 540]],
        type: 'w'
    },
    {
        id: 1,
        position: [[420, 240], [300, 460]],
        type: 'r'
    }
])

/**
 * Select
 */

const inRange = (low, high) => allPass([
    x => x > low, 
    x => x < high
])

const diff = (a, b) => a - b

const moveLine = (line, [startMouseX, startMouseY]) => ([mouseX, mouseY]) => {
    const [startPoint, endPoint] = line.position
    const [startX, startY] = startPoint
    const [endX, endY] = endPoint

    const moveX = (startMouseX - mouseX) * -1
    const moveY = (startMouseY - mouseY) * -1

    return {
        ...line,
        position: [[moveX + startX, moveY + startY], [moveX + endX, moveY + endY]]
    }
}

const select = mouseDown.pipe(
    withLatestFrom(lines),
    map(([[mouseX, mouseY], lines]) => lines.find(({position}) => {
        const [startPoint, endPoint] = position
        const [startX, startY] = startPoint
        const [endX, endY] = endPoint
        const xRange = sort(diff, [startX, endX])
        const yRange = sort(diff, [startY, endY])
        return inRange(...xRange)(mouseX) && inRange(...yRange)(mouseY)
    })),
    skipWhile(result => result === undefined),
).pipe(repeat())

const move = select.pipe(
    withLatestFrom(mouseDown),
    switchMap(([component, mouseDown]) => mouseMove.pipe(
        flatMap(mouseMove => of(moveLine(component, mouseDown)(mouseMove))),
        takeUntil(mouseUp)
    ))
).pipe(repeat())

const lineChange = merge(move).pipe(
    withLatestFrom(lines),
    map(([newLine, lines]) => lines.map(line => newLine.id === line.id ? newLine : line))
)

lineChange.subscribe(newLines => lines.next(newLines))

lines.subscribe((lines) => {
    clearCanvas()
    lines.map((line) => drawLine(line))
})