import { fromEvent, merge, BehaviorSubject } from 'rxjs';
import { map, takeUntil, concatMap, startWith, filter, withLatestFrom, distinctUntilChanged } from 'rxjs/operators';
import { drawLine, clearCanvas, rect } from './canvas'

/**
 * Key events
 */

const anyKeyPresses = fromEvent(document, 'keypress').pipe(
    map(event => event.key)
)

const keyPressed = (key) => anyKeyPresses.pipe(filter(pressedKey => pressedKey === key))

const keyR = keyPressed('r')
const keyW = keyPressed('w')

const keys = merge(keyR, keyW).pipe(startWith('w'))

/**
 * Grid
 */

const gridPosition = ([x, y]) => {
    const grid = 20
    const column = Math.floor((x + (grid / 2)) / grid)
    const row = Math.floor((y + (grid / 2)) / grid)
    return [column * grid, row * grid]
}

const gridPositionChanged = ([x, y], [xNew, yNew]) => x === xNew && y === yNew

/**
 * Mouse events
 */

const mouseDown = fromEvent(window, 'mousedown').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top]),
    map(gridPosition),
    distinctUntilChanged(gridPositionChanged)
)
const mouseMove = fromEvent(window, 'mousemove').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top]),
    map(gridPosition),
    distinctUntilChanged(gridPositionChanged)
)
const mouseUp = fromEvent(window, 'mouseup').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

/**
 * Lines
 */

const lines = new BehaviorSubject([
    {
        id: 0,
        position: [[ 340, 580 ], [ 500, 540 ]],
        type: 'w'
    },
    {
        id: 1,
        position: [[420, 240], [300,460]],
        type: 'r'
    }
])

/**
 * Draw
 */

const lineDraw = mouseDown.pipe(
    withLatestFrom(keys),
    withLatestFrom(lines),
    concatMap(([[mouseDown, key], lines]) => mouseMove.pipe(
        map(endPoint => ({
            id: lines.length + 1,
            position: [mouseDown, endPoint],
            type: key
        })),
        takeUntil(mouseUp)
    ))
)

const lineChange = merge(lineDraw).pipe(
    withLatestFrom(lines),
    map(([newLine, lines]) => [...lines.filter(({id}) => id !== newLine.id), newLine])
)

lineChange.subscribe(newLines => lines.next(newLines))

lines.subscribe((lines) => {
    console.log(lines)
    clearCanvas()
    lines.map((line) => drawLine(line))
})