import { fromEvent, merge, BehaviorSubject, of, iif } from 'rxjs';
import { map, takeUntil, shareReplay, concatMap, tap, startWith, filter, distinctUntilChanged, mergeMap, skipWhile, repeat, flatMap, withLatestFrom, switchMap } from 'rxjs/operators';
import { sort, allPass } from 'ramda'
import { drawLine, clearCanvas, rect } from './canvas'

/**
 * Key events
 */

const anyKeyPresses = fromEvent(document, 'keydown').pipe(
    map(event => event.key)
)

const keyPressed = (key) => anyKeyPresses.pipe(filter(pressedKey => pressedKey === key))

const keyR = keyPressed('r')
const keyW = keyPressed('w')
const keyEscape = keyPressed('Escape')

const componentKeys = merge(keyR, keyW).pipe(
    shareReplay(1)
)

const selectMode = merge(
    componentKeys.pipe(map(() => 'draw')), 
    keyEscape.pipe(map(() => 'select'))
).pipe(startWith('select'))

/**
 * Grid
 */

const gridPosition = ([x, y]) => {
    const grid = 20
    const column = Math.floor((x + (grid / 2)) / grid)
    const row = Math.floor((y + (grid / 2)) / grid)
    return [column * grid, row * grid]
}

const gridPositionChanged = ([x, y], [xNew, yNew]) => x === xNew && y === yNew

/**
 * Mouse events
 */

const mouseDown = fromEvent(window, 'mousedown').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top]),
    map(gridPosition)
)
const mouseMove = fromEvent(window, 'mousemove').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top]),
    map(gridPosition),
    distinctUntilChanged(gridPositionChanged)
)
const mouseUp = fromEvent(window, 'mouseup').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)

/**
 * Lines
 */

const lines = new BehaviorSubject([
    {
        id: 0,
        position: [[340, 580], [500, 540]],
        type: 'w'
    },
    {
        id: 1,
        position: [[420, 240], [300, 460]],
        type: 'r'
    }
])

/**
 * Draw (draw.js)
 */

const draw = mouseDown.pipe(
    withLatestFrom(componentKeys),
    withLatestFrom(lines),
    concatMap(([[mouseDown, key], lines]) => mouseMove.pipe(
        map(endPoint => ({
            id: lines.length + 1,
            position: [mouseDown, endPoint],
            type: key
        })),
        takeUntil(mouseUp)
    ))
)

/**
 * Select and move (drag.js)
 */

const inRange = (low, high) => allPass([
    x => x > low, 
    x => x < high
])

const diff = (a, b) => a - b

const moveLine = (line, [startMouseX, startMouseY]) => ([mouseX, mouseY]) => {
    const [startPoint, endPoint] = line.position
    const [startX, startY] = startPoint
    const [endX, endY] = endPoint

    const moveX = (startMouseX - mouseX) * -1
    const moveY = (startMouseY - mouseY) * -1

    return {
        ...line,
        position: [[moveX + startX, moveY + startY], [moveX + endX, moveY + endY]]
    }
}

const select = mouseDown.pipe(
    withLatestFrom(lines),
    map(([[mouseX, mouseY], lines]) => lines.find(({position}) => {
        const [startPoint, endPoint] = position
        const [startX, startY] = startPoint
        const [endX, endY] = endPoint
        const xRange = sort(diff, [startX, endX])
        const yRange = sort(diff, [startY, endY])
        return inRange(...xRange)(mouseX) && inRange(...yRange)(mouseY)
    })),
    filter(result => result !== undefined),
).pipe(repeat())

const move = select.pipe(
    withLatestFrom(mouseDown),
    switchMap(([component, mouseDown]) => mouseMove.pipe(
        flatMap(mouseMove => of(moveLine(component, mouseDown)(mouseMove))),
        takeUntil(mouseUp)
    ))
).pipe(repeat())

/**
 * Switch between selection and create mode
 */

const mode = selectMode.pipe(
    switchMap(mode => iif(() => mode === 'select', move, draw))
)

/**
 * Store
 */

const lineChange = mode.pipe(
    withLatestFrom(lines),
    map(([newLine, lines]) => [...lines.filter(({id}) => id !== newLine.id), newLine])
)

lineChange.subscribe(newLines => lines.next(newLines))

/**
 * Render
 */

lines.subscribe((lines) => {
    clearCanvas()
    lines.map((line) => drawLine(line))
})