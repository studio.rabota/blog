/**
 * Key events
 */

const anyKeyPresses = fromEvent(document, 'keypress').pipe(
    map(event => event.key)
)

const keyPressed = (key) => anyKeyPresses.pipe(filter(pressedKey => pressedKey === key))

const keyR = keyPressed('r')
const keyW = keyPressed('w')

const keys = merge(keyR, keyW).pipe(startWith('w'))


/**
 * Grid
 */

const gridPosition = ([x, y]) => {
    const grid = 20
    const column = Math.floor((x + (grid / 2)) / grid)
    const row = Math.floor((y + (grid / 2)) / grid)
    return [column * grid, row * grid]
}

const gridPositionChanged = ([x, y], [xNew, yNew]) => x === xNew && y === yNew

/**
 * Mouse events
 */

const mouseDown = fromEvent(window, 'mousedown').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top]),
    map(gridPosition),
    distinctUntilChanged(gridPositionChanged)
)
const mouseMove = fromEvent(window, 'mousemove').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top]),
    map(gridPosition),
    distinctUntilChanged(gridPositionChanged)
)
const mouseUp = fromEvent(window, 'mouseup').pipe(
    map((event) => [event.x - rect.left, event.y - rect.top])
)