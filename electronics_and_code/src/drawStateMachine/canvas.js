/**
 * Canvas
 */

const canvas = document.getElementById('circuit-canvas')
export const rect = canvas.getBoundingClientRect()
canvas.width = 600;
canvas.height = 600;
const context = canvas.getContext('2d')
context.lineWidth = 5
context.fillStyle = 'black'

export const circle = (x, y) => {
    const circle = new Path2D();
    const radius = 5
    const startAngle = 0
    const endAngle = Math.PI * 2
    circle.arc(x, y, radius, startAngle, endAngle)
    return circle
}

export const line = (start, end) => {
    const line = new Path2D();
    line.moveTo(start.x, start.y)
    line.lineTo(end.x, end.y)
    return line
}

export const pointOnPath = (mouse, [start, end]) => {
    return context.isPointInStroke(line(start, end), mouse.x, mouse.y)
}

export const pointOnDot = (mouse, dot) => {
    return context.isPointInPath(circle(dot.x, dot.y), mouse.x, mouse.y)
}

export const drawCircles = (position) => {
    const [start, end] = position
    circle(start.x, start.y)
    circle(end.x, end.y)
}
 
export const drawLine = ({position, componentType, stateValue }) => {
    const [start, end] = position

    context.strokeStyle = 'green'

    if (componentType === 'r') {
        context.strokeStyle = 'blue'
    } 

    if (componentType === 'w') {
        context.strokeStyle = 'pink'
    }

    if (stateValue === 'hover') {
        context.strokeStyle = 'orange'
    }

    if (stateValue === 'click') {
        context.strokeStyle = 'red'
    }

    if (stateValue === 'drag') {
        context.strokeStyle = 'purple'
    }

    context.stroke(line(start, end))
    context.fill(circle(start.x, start.y))
    context.fill(circle(end.x, end.y))
}

export const clearCanvas = () => {
    context.clearRect(0, 0, canvas.width, canvas.height)
}