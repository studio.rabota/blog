import { from } from 'rxjs';
import { interpret } from 'xstate';
import { drawLine, clearCanvas } from './canvas';
import { diagramMachine } from './machineDiagram';

// TODO:
// Refactor draggind of dots a bit?
// Refactor canvas drawLine color selection

const service = interpret(diagramMachine).start()

service.send('ADD', { componentType: 'w', position: [{x: 420, y: 240}, {x: 300, y: 460}] })

const state = from(service)

state.subscribe(state => {
    clearCanvas()

    state.context.components.forEach((child, index) => {
        drawLine({index, stateValue: child.state.value, ...child.state.context })
    })
})