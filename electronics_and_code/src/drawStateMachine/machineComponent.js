import { merge } from 'rxjs';
import { Machine, assign } from 'xstate';
import { both, complement } from 'ramda'
import { 
    keys, 
    gridMouseDown, gridMouseMove, gridMouseUp, mouseMove, 
    condDrawKey, condEscapeDrawKey, condPointOnPath, condPointOnStartDot, condPointOnEndDot
} from './userInput';

const moveLine = (linePosition, startMouse) => (mouse) => {
    const [startPoint, endPoint] = linePosition

    const moveX = (startMouse.x - mouse.x) * -1
    const moveY = (startMouse.y - mouse.y) * -1

    return [{ x: moveX + startPoint.x, y: moveY + startPoint.y }, { x: moveX + endPoint.x, y: moveY + endPoint.y}]
}

const assignClickPosition = assign({
    clickPosition: (_, {x, y}) => ({ x, y }),
    startDragPosition: (context) =>  context.position
})

const assignPosition = assign({
    position: (context, event) => moveLine(context.startDragPosition, context.clickPosition)(event)
})

const assignStartDotPosition = assign({
    position: (context, {x, y}) => {
        const [_, end] = context.position
        return [{x, y}, end]
    }
})

const assignEndDotPosition = assign({
    position: (context, {x, y}) => {
        const [start] = context.position
        return [start, {x, y}]
    }
})

export const createComponentMachine = ({componentType = 'w', position, initial = "idle"}) => {
    return Machine({
        id: "component",
        initial,
        context: {
            componentType,
            position,
            clickPosition: {x: 0, y: 0},
            startDragPosition: [{x: 0, y: 0}, {x: 0, y: 0}]
        },
        states: {
            idle: {
                invoke: {
                    src: () => merge(mouseMove, keys)
                },
                on: {
                    mousemove: {
                        target: 'hover',
                        cond: condPointOnPath
                    },
                    keydown: {
                        target: 'locked',
                        cond: condDrawKey
                    }
                }
            },
            locked: {
                invoke: {
                    src: () => keys
                },
                on: {
                    keydown: {
                        target: 'idle',
                        cond: condEscapeDrawKey
                    }
                }
            },
            hover: {
                invoke: {
                    src: () => merge(mouseMove, gridMouseDown), 
                },
                on: {
                    mousemove: {
                        target: 'idle',
                        cond: complement(condPointOnPath)
                    },
                    mousedown: {
                        target: 'click',
                        actions: assignClickPosition,
                    }
                }
            },
            click: {
                invoke: {
                    src: () => merge(gridMouseMove, gridMouseUp), 
                },
                on: {
                    mousemove: [
                        {
                            target: 'drag',
                            cond: both(complement(condPointOnEndDot), complement(condPointOnStartDot)),
                            actions: assignPosition,
                        },
                        {
                            target: 'dragStartDot',
                            cond: condPointOnStartDot,
                            actions: assignStartDotPosition,
                        },
                        {
                            target: 'dragEndDot',
                            cond: condPointOnEndDot,
                            actions: assignEndDotPosition,
                        }
                    ],
                    mouseup: 'idle'
                }
            },
            drag: {
                invoke: {
                    src: () => merge(gridMouseMove, gridMouseUp), 
                },
                on: {
                     mousemove: {
                        internal: true,
                        actions: assignPosition
                    },
                    mouseup: 'idle'
                }
            },
            dragNewComponentDot: {
                invoke: {
                    src: () => merge(gridMouseMove, gridMouseUp), 
                },
                on: {
                    mousemove: {
                        internal: true,
                        actions: assignEndDotPosition
                    },
                    mouseup: 'locked'
                }
            },
            dragStartDot: {
                invoke: {
                    src: () => merge(gridMouseMove, gridMouseUp), 
                },
                on: {
                    mousemove: {
                        internal: true,
                        actions: assignStartDotPosition
                    },
                    mouseup: 'idle'
                }
            },
            dragEndDot: {
                invoke: {
                    src: () => merge(gridMouseMove, gridMouseUp), 
                },
                on: {
                    mousemove: {
                        internal: true,
                        actions: assignEndDotPosition
                    },
                    mouseup: 'idle'
                }
            },
        }
    });
};
