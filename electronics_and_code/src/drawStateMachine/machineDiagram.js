import { merge } from 'rxjs';
import { Machine, assign, spawn, send } from 'xstate';
import { keys, gridMouseDown, condDrawKey, condEscapeDrawKey } from './userInput';
import { createComponentMachine } from './machineComponent';

// State Machines

const assignDrawMode = assign((_, { key }) => ({ drawMode: key }))

const sendAddComponentMessage = (context, {x, y}) => ({ 
    type: 'ADD', 
    position: [{x, y}, {x, y}],
    componentType: context.drawMode,
    initial: 'dragNewComponentDot' 
})

export const diagramMachine = Machine(
    {
        id: 'diagram',
        initial: 'idle',
        context: {
            components: [],
            drawMode: 'w'
        },
        states: { 
            idle: {
                invoke: {
                    src: () => keys, 
                },
                on: {
                    keydown: {
                        target: 'draw',
                        cond: condDrawKey,
                        actions: assignDrawMode
                    }
                }
            }, 
            draw: {
                invoke: {
                    src: () => merge(gridMouseDown, keys), 
                },
                on: {
                    mousedown: { 
                        actions: send(sendAddComponentMessage)
                    },
                    keydown: [
                        {
                            target: 'idle',
                            cond: condEscapeDrawKey
                        }, 
                        {
                            target: 'draw',
                            cond: condDrawKey,
                            actions: assignDrawMode
                        }
                    ],
                }
            }
        },
        on: {
            ADD: {
                actions: assign((context, event) => ({
                    components: [
                        ...context.components,
                        spawn(createComponentMachine(event), { sync: true })
                    ]
                }))
            }
        }
    }
)