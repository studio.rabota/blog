import { fromEvent, merge } from 'rxjs';
import { map, filter, distinctUntilChanged } from 'rxjs/operators';
import { rect, pointOnPath, pointOnDot } from './canvas'

const gridPosition = ({type, x, y}) => {
    const grid = 20
    const column = Math.floor((x + (grid / 2)) / grid)
    const row = Math.floor((y + (grid / 2)) / grid)
    return { type, x: column * grid, y: row * grid }
}

const gridPositionChanged = (prev, next) => prev.x === next.x && prev.y === next.y

// Keys

const anyKeyPresses = fromEvent(document, 'keydown')

const keyPressed = (key) => anyKeyPresses.pipe(filter(event => event.key === key))

const keyR = keyPressed('r')
const keyW = keyPressed('w')
const keyEscape = keyPressed('Escape')

export const keys = merge(keyR, keyW, keyEscape)

// Mouse

const canvasPositionEvent = ({x, y, type}) => ({ type, x: x - rect.left, y: y - rect.top })

export const mouseMove = fromEvent(window, 'mousemove').pipe(
    map(canvasPositionEvent)
)

export const gridMouseMove = mouseMove.pipe(
    map(gridPosition),
    distinctUntilChanged(gridPositionChanged)
)

export const gridMouseDown = fromEvent(window, 'mousedown').pipe(
    map(canvasPositionEvent),
    map(gridPosition)
)

export const gridMouseUp = fromEvent(window, 'mouseup').pipe(
    map(canvasPositionEvent),
    map(gridPosition)
)

// Conditions

export const condPointOnPath = (context, event) => pointOnPath(event, context.position)
export const condPointOnStartDot = ({ clickPosition, position: [start] }) => pointOnDot(clickPosition, start)
export const condPointOnEndDot = ({ clickPosition, position: [_, end] }) => pointOnDot(clickPosition, end)
export const condDrawKey = (_, { key }) => ['w', 'r'].includes(key)
export const condEscapeDrawKey = (_, { key }) => key === 'Escape'