import cytoscape from 'cytoscape'

var cy = cytoscape({

    container: document.getElementById('cy'), // container to render in
  
    // elements: [ // list of graph elements to start with
    //   { // node a
    //     data: { id: 'a' }
    //   },
    //   { // node b
    //     data: { id: 'b' }
    //   },
    //   { // edge ab
    //     data: { id: 'ab', weight: '1', source: 'a', target: 'b' }
    //   },
    //   { // edge ab
    //     data: { id: 'ba', weight: '-1', source: 'b', target: 'a' }
    //   }
    // ],
  
    elements: [
        {
            data: { id: '0' }
        },
        {
            data: { id: '1' }
        },
        {
            data: { id: '2' }
        },
        {
            data: { id: 'R1', weight: '10', source: '1', target: '0' }
        },
        {
            data: { id: 'R2', weight: '1000', source: '1', target: '2' }
        },
        {
            data: { id: 'R3', weight: '1000', source: '2', target: '0' }
        }
    ],

    style: [ // the stylesheet for the graph
      {
        selector: 'node',
        style: {
          'background-color': '#666',
          'label': 'data(id)'
        }
      },
  
      {
        selector: 'edge',
        style: {
          'width': 3,
          'line-color': '#ccc',
          'target-arrow-color': '#ccc',
          'target-arrow-shape': 'triangle',
          'curve-style': 'bezier',
          'label': 'data(weight)'
        }
      }
    ],
  
    // layout: {
    //   name: 'grid',
    //   rows: 1
    // }
  
  });