import cytoscape from 'cytoscape'
import { equals, uniq } from 'ramda'

import { style } from './graph_visuals'

const canvas = document.getElementById('circuit-canvas')
canvas.width = 800;
canvas.height = 800;
const context = canvas.getContext('2d')

const diagram = [
    {
        position: [{x: 20, y: 300}, {x: 20, y: 10}],
        data: { id: 'IS', weight: '1' }
    },
    {
        position: [{x: 20, y: 10}, {x: 200, y: 10}],
        data: { id: 'line1' }
    },
    {
        position: [{x: 200, y: 10}, {x: 200, y: 300}],
        data: { id: 'R1', weight: '10' }
    },
    {
        position: [{x: 200, y: 10}, {x: 600, y: 10}],
        data: { id: 'R2', weight: '1000' }
    },
    {
        position: [{x: 600, y: 10}, {x: 600, y: 300}],
        data: { id: 'R3', weight: '1000' }
    },
    {
        position: [{x: 600, y: 300}, {x: 200, y: 300}],
        data: { id: 'line2' }
    },
    {
        position: [{x: 200, y: 300}, {x: 20, y: 300}],
        data: { id: 'line3' }
    }
]

/**
 * LINEAR_DC_CKT.CIR - SIMPLE CIRCUIT FOR NODAL ANALYSIS
*
IS	0	1	DC	1A
*
R1	1	0	10
R2	1	2	1K
R3	2	0	1K
*
* ANALYSIS
.TRAN 	1MS  10MS
* VIEW RESULTS
.PRINT	TRAN 	V(1) V(2)
.PROBE
.END
 */



diagram.forEach(({position}) => {
    const [start, end] = position
    context.beginPath()
    context.moveTo(start.x, start.y)
    context.lineTo(end.x, end.y)
    context.stroke()
})

var cy = cytoscape({
    container: document.getElementById('cy-circuit'), 
    elements: [],
    style
})
cy.zoomingEnabled( false );

const allNodes = diagram.flatMap(({position}) => position)
const uniqNodes = uniq(allNodes)

uniqNodes.forEach((position, index) => {
    cy.add([
        { 
            group: 'nodes',
            data: { id: index },
            position
        }
    ])
})

diagram.forEach(({data, position}) => {
    const [start, end] = position
    const source = uniqNodes.findIndex(equals(start))
    const target = uniqNodes.findIndex(equals(end))
    cy.add([
        { 
            group: 'edges',
            data: {
                ...data,
                source,
                target
            }
        }
    ])
})