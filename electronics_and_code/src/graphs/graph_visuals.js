import { matrix, multiply, inv, sum } from 'mathjs'
import cytoscape from 'cytoscape'

export const style = [
    {
        selector: 'node',
        style: {
        'background-color': 'hotpink',
        'label': 'data(id)'
        }
    },
    {
        selector: 'edge',
        style: {
        'width': 3,
        'line-color': 'blue',
        'target-arrow-color': 'orange',
        'target-arrow-shape': 'triangle',
        'curve-style': 'bezier',
        'label': 'data(weight)'
        }
    }
]

const diagramGraph = [
    {
        data: { id: '0' }
    },
    {
        data: { id: '1' }
    },
    {
        data: { id: '2' }
    },
    {
        data: { id: 'R1', weight: '10', source: '0', target: '1' }
    },
    {
        data: { id: 'R2', weight: '1000', source: '1', target: '2' }
    },
    {
        data: { id: 'R3', weight: '1000', source: '2', target: '0' }
    }
]

var cy = cytoscape({
    container: document.getElementById('cy'), 
    elements: diagramGraph,
    style
})
cy.zoomingEnabled( false );

const nodes = [1, 2]

const arrayForMatrix = nodes.map(node => {
    const cyNode = cy.$(`#${node}`)

    // Draw nodeGraph
    const cyNodeGraph = cytoscape({
        container: document.getElementById(`cy-node-graph-${node}`), 
        elements: [],
        style
    })

    cyNodeGraph.add(cyNode)
    cyNodeGraph.add(cyNode.neighborhood())
    cyNodeGraph.zoomingEnabled( false )

    // Draw biDirectionalNodeGraph
    const cyNodeGraphBiDirectional = cytoscape({
        container: document.getElementById(`cy-node-graph-${node}-birectional`), 
        elements: [],
        style
    })

    cyNodeGraphBiDirectional.add(cyNode)
    cyNodeGraphBiDirectional.add(cyNode.neighborhood().filter(component => component.isNode()))
    cyNodeGraphBiDirectional.zoomingEnabled( false )

    cyNode.neighborhood().filter(component => component.isEdge()).forEach(edge => {
        if (node == edge.data('target')) {
            cyNodeGraphBiDirectional.add([
                { 
                    group: 'edges',
                    data: { 
                        source: edge.data('source'),
                        target: edge.data('target'),
                        weight: -edge.data('weight'),
                        id: `negative-${edge.data('id')}`
                    } 
                },
                { 
                    group: 'edges',
                    data: { 
                        source: edge.data('target'),
                        target: edge.data('source'),
                        weight: edge.data('weight'),
                        id: `positive-${edge.data('id')}`
                    } 
                }
            ])
        } else {
            cyNodeGraphBiDirectional.add([
                { 
                    group: 'edges',
                    data: { 
                        source: edge.data('source'),
                        target: edge.data('target'),
                        weight: edge.data('weight'),
                        id: `positive-${edge.data('id')}`
                    } 
                },
                { 
                    group: 'edges',
                    data: { 
                        source: edge.data('target'),
                        target: edge.data('source'),
                        weight: -edge.data('weight'),
                        id: `negative-${edge.data('id')}`
                    } 
                }
            ])
        }
    })

    // Draw outgoersNodeGraph
    const outgoersEdges = nodes.map(innerNode => {
        const cyNodeGraphBiDirectionalNode = cyNodeGraphBiDirectional.$(`#${innerNode}`)
        const outgoers = cyNodeGraphBiDirectionalNode.outgoers()
        
        const cyNodeGraphOutgoers = cytoscape({
            container: document.getElementById(`cy-node-graph-${node}-outgoers-node-${innerNode}`), 
            elements: [],
            style
        })

        cyNodeGraphOutgoers.add(cyNodeGraphBiDirectionalNode)
        cyNodeGraphOutgoers.add(outgoers)
        cyNodeGraphOutgoers.zoomingEnabled( false )

        return outgoers.filter(component => component.isEdge())
    })

    const conductanceCalc = outgoersEdges.map(edge => sum(edge.map(edge => 1/edge.data('weight'))))

    return conductanceCalc
})

console.log('arrayForMatrix', arrayForMatrix)

const GfromGraph = matrix(arrayForMatrix)

const invG = inv(GfromGraph)

const currentSource = 1
const i = matrix([currentSource, 0])

const result = multiply(invG, i)

console.log('result', result)