const Sum = x => ({
    x,
    concat: ({x: y}) => Sum(x + y),
    inspect: () => `Sum(${x})`
})

const Divide = x => ({
    x,
    concat: ({x: y}) => Divide(x / y),
    fold: () => x,
    inspect: () => `Divide(${x})`
})

const Multiply = x => ({
    x,
    concat: ({x: y}) => Divide(x * y),
    fold: () => x,
    inspect: () => `Divide(${x})`
})

console.log(Sum(1).concat(Sum(2)).inspect())
console.log(Divide(10).concat(Divide(2)).fold())
console.log(Multiply(10).concat(Multiply(2)).concat(Divide(5)).fold())