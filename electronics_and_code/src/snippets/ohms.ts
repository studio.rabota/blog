const calculateCurrent = (V: Voltage, R: Resistance): Current => new Current(V.value / R.value)
const calculateVoltage = (I: Current, R: Resistance): Voltage => new Voltage(I.value * R.value)
const calculateResistance = (V: Voltage, I: Current): Resistance => new Resistance(V.value / I.value)

class Metric<T> {
    unit: T
    #value: number

    constructor(value: number = 0) {
        this.#value = value;
    }

    set value (value) {
        this.#value = value
    }

    get value () {
        return this.#value
    }

    set milli (value) {
        this.value = value / 1000
    }

    get milli () {
        return this.value * 1000
    }
}

class Resistance extends Metric<'resistance'> {
    constructor(value: number = 0){
        super(value);
    }
}

class Voltage extends Metric<'voltage'> {
    constructor(value: number = 0){
        super(value);
    }
}

class Current extends Metric<'current'> {
    constructor(value: number = 0){
        super(value);
    }
}

const voltage = new Voltage(9)
const current = new Current(10)

const resistance = calculateResistance(voltage, current)

console.log(resistance.value)
console.log(resistance.milli)