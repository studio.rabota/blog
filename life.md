# Young chump

Young chump. Programming is hard. Programming is easy. Addicting to some, dead boring to others. It is like bumping your head against a wall, and at the same time receiving small shots of joy. In fact, the number one trade to develop as a developer is the ability of mentally dealing with "error", and knowing you're going to fix the problem.

Programming is easy. Get to know the function, the array, the object, voila you can now write basic software [examples]. 

Programming is hard. The code will grow, the amount of people working on the codebase will change. From making your own to dealing with teams, complexity and risk. You will now need to know how to deal with testing, code reviews, entropy and people. Some feelings of imposter syndrom are bound to appear and will never really go away.

Programming changes as one ages. While the act stays the same, it is ones view on love or like for writing in a computer language which changes. Most developers grow up as kids typing away and being in awh of what appears on the screen. As the kids grows up and must act like an adult, working to pay rent, the love of programming is diminished and dreams of other careers are in ones mind. However rarely do I meet a programmer who does not like programming and who does not feel bliss for when a programming problem is solved.

Which "real" problem do you want to solve? This what you must consider first when starting programming. Do you care about global warming or how to feed the world, perhaps something smaller? [real examples]. It is your drive to solve a "real" problem, which will make you overcome "programming" problems. 

Code is a mere tool. A powerful tool which has changed the world for better and worse. Mastering this tool will require effort and pain. However, spend enough time coding and you will be able to solve many problems close to your heart [example of problems]. So, always ask yourself, which problem do you want to solve first?

....

- The progamming problem

- The real problem