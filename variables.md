``` terminal
> I = V/R
```

`Uncaught TypeError: Assignment to constant variable.`, mmmh. This is correct, in fact, the computers is always correct (better get used to it). So what did we do wrong?

In modern day Javscript we have 3 types of variables, `let`, `const`  and `var`. 

- `var` is from back in the day, feel free to forget about this one right away. You might come accross `var` in some old codebases and transpiled code. 
- `let` is a variable which can change over time. You could for example write `let I = undefined` and later on assign the number `10` to `I`, like `let I = 10`.
- `const` is the defacto standard for declaring variables. It is constant and can only be assigned once. Always go with `const`, unless you really have no other option.

Let's get back to our terminal. We just want to see the result for our calculation. We can do this by not assigning the value and letting the REPL display the result.
