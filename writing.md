## How to write a function

Code can be written in many ways. In that way it is like human language, only a little bit more strict. However as with human language, there is a right way of writing code and incorrect or less optimable ways of communicating.

The number one reason to write code in a certain way is readability. Readability might not be of consern for one function, however when code grows, readability becomes important.

The number two reason is re-usability. And sometimes, however not often, a reason could be performance.

We could have written the code as follows:

``` javascript
const calculate = (V, I, R) => {
    let result = undefined;

    if (R === undefined) {
        return V/I
    }
    if (I === undefined) {
        ...
    }
}
```

Can you tell me what is wrong with the code above?

...

Or we could write the function as:

``` javascript
let V = 9;
let I = 10;
let R = undefined
const calculateResistance = (R, V, I) => R = V/I
```

Can you tell me what is wrong with the code above?

...

``` javascript
const calculateCurrent = (V, R) => V/R
```

Notice that we do not assign the variable to `I` as in the following code. This is considered bad practice. A function in general returns a value and should not modify external variables. 

``` javascript
// Never ever do something like this
let I = undefined
const calculateCurrent = (V, R) => I = V/R
```

Writing the function with an object as params can sometimes improver the readability.
